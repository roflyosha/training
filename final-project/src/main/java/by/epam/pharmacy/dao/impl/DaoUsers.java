package by.epam.pharmacy.dao.impl;

import by.epam.pharmacy.dao.*;
import by.epam.pharmacy.entity.Status;
import by.epam.pharmacy.entity.User;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by a.gladkikh on 17/09/16.
 */
public class DaoUsers implements CrudDao<User> {
    private static final Logger LOGGER = Logger.getLogger(DaoUsers.class);
    private static final DaoUsers instance = new DaoUsers();
    private final String READ = "SELECT\n" +
            "  u.id,\n" +
            "  u.login,\n" +
            "  u.password,\n" +
            "  u.name,\n" +
            "  u.surname,\n" +
            "  st.id   AS status_id,\n" +
            "  st.name AS status\n" +
            "FROM user u INNER JOIN status st ON u.status_id = st.id\n";
    private final String INSERT = "INSERT INTO user (login, password, name, surname, status_id)\n" +
            "VALUES (?, ?, ?, ?, ?);";
    private final String UPDATE = "UPDATE user\n" +
            "SET login = ?, password = ?, name = ?, surname = ?, status_id = ?\n" +
            "WHERE id = ?;";
    private final String DELETE = "DELETE FROM user\n" +
            "WHERE id = ?;";
    private final String GET_USER_BY_LOGIN_AND_PASSWORD = "SELECT" +
            "  u.id,\n" +
            "  u.login,\n" +
            "  u.password,\n" +
            "  u.name,\n" +
            "  u.surname,\n" +
            "  st.id   AS status_id,\n" +
            "  st.name AS status\n" +
            "FROM user u INNER JOIN status st ON u.status_id = st.id WHERE login = ? AND password = ?;";
    private ConnectionManager cm = null;
    private Connection connection = null;
    private PreparedStatement preparedStatement;

    private DaoUsers() {
    }

    public static DaoUsers getInstance() {
        return instance;
    }

    @Override
    public void create(User entity) throws DaoException {
        try {
            cm = ConnectionManager.getInstance();
            connection = cm.getConnection();
            preparedStatement = connection.prepareStatement(INSERT);
            preparedStatement.setString(1, entity.getLogin());
            preparedStatement.setString(2, entity.getPassword());
            preparedStatement.setString(3, entity.getName());
            preparedStatement.setString(4, entity.getSurname());
            preparedStatement.setLong(5, entity.getStatus().getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Can't execute prepared statement.", e);
        } catch (ConnectionException e) {
            throw new DaoException("Can't get connection to the database", e);
        } finally {
            ConnectionManager.closeConnectionAndPreparedStatement(connection, preparedStatement);
        }
    }

    @Override
    public List<User> readAll() throws DaoException {
        List<User> list = null;
        try {
            cm = ConnectionManager.getInstance();
            connection = cm.getConnection();
            preparedStatement = connection.prepareStatement(READ);
            ResultSet rs = preparedStatement.executeQuery();
            list = new ArrayList<>();
            while (rs.next()) {
                Long id = rs.getLong(1);
                String login = rs.getString(2);
                String password = rs.getString(3);
                String name = rs.getString(4);
                String surname = rs.getString(5);
                Long status_id = rs.getLong(6);
                String status_name = rs.getString(7);
                Status status = new Status();
                status.setId(status_id);
                status.setName(status_name);
                User user = new User();
                user.setId(id);
                user.setLogin(login);
                user.setPassword(password);
                user.setName(name);
                user.setSurname(surname);
                user.setStatus(status);
                list.add(user);
            }
        } catch (SQLException e) {
            throw new DaoException("Can't execute prepared statement.", e);
        } catch (ConnectionException e) {
            throw new DaoException("Can't get connection to the database", e);
        } finally {
            ConnectionManager.closeConnectionAndPreparedStatement(connection, preparedStatement);
        }
        return list;
    }

    @Override
    public void update(User entity) throws DaoException {
        try {
            cm = ConnectionManager.getInstance();
            connection = cm.getConnection();
            preparedStatement = connection.prepareStatement(UPDATE);
            preparedStatement.setString(1, entity.getLogin());
            preparedStatement.setString(2, entity.getPassword());
            preparedStatement.setString(3, entity.getName());
            preparedStatement.setString(4, entity.getSurname());
            preparedStatement.setLong(5, entity.getStatus().getId());
            preparedStatement.setLong(6, entity.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Can't execute prepared statement.", e);
        } catch (ConnectionException e) {
            throw new DaoException("Can't get connection to the database", e);
        } finally {
            ConnectionManager.closeConnectionAndPreparedStatement(connection, preparedStatement);
        }
    }

    @Override
    public void delete(User entity) throws DaoException {
        try {
            cm = ConnectionManager.getInstance();
            connection = cm.getConnection();
            preparedStatement = connection.prepareStatement(DELETE);
            preparedStatement.setLong(1, entity.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Can't execute prepared statement.", e);
        } catch (ConnectionException e) {
            throw new DaoException("Can't get connection to the database", e);
        } finally {
            ConnectionManager.closeConnectionAndPreparedStatement(connection, preparedStatement);
        }
    }

    public User getUserByLoginAndPassword(String login, String password) throws DaoException {
        User user = null;
        try {
            cm = ConnectionManager.getInstance();
            connection = cm.getConnection();
            preparedStatement = connection.prepareStatement(GET_USER_BY_LOGIN_AND_PASSWORD);
            preparedStatement.setString(1, login);
            preparedStatement.setString(2, User.hashFromPassword(password));
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                user = new User();
                user.setId(rs.getLong(1));
                user.setLogin(rs.getString(2));
                user.setPassword(rs.getString(3));
                user.setName(rs.getString(4));
                user.setSurname(rs.getString(5));
                Status status = new Status();
                status.setId(rs.getLong(6));
                status.setName(rs.getString(7));
                user.setStatus(status);
            }
        } catch (SQLException e) {
            throw new DaoException("Can't execute prepared statement.", e);
        } catch (ConnectionException e) {
            throw new DaoException("Can't get connection to the database", e);
        } finally {
            ConnectionManager.closeConnectionAndPreparedStatement(connection, preparedStatement);
        }
        return user;
    }

}
