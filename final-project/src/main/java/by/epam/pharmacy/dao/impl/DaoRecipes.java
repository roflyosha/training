package by.epam.pharmacy.dao.impl;

import by.epam.pharmacy.dao.ConnectionException;
import by.epam.pharmacy.dao.ConnectionManager;
import by.epam.pharmacy.dao.CrudDao;
import by.epam.pharmacy.dao.DaoException;
import by.epam.pharmacy.entity.Medication;
import by.epam.pharmacy.entity.Recipe;
import by.epam.pharmacy.entity.User;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by a.gladkikh on 30/09/16.
 */
public class DaoRecipes implements CrudDao<Recipe> {
    private static final Logger LOGGER = Logger.getLogger(Recipe.class);
    private static final DaoRecipes instance = new DaoRecipes();
    private final String INSERT = "INSERT INTO recipe (User_id, Medication_id, free, " +
            "expiration_date, need_extension) VALUES (?, ?, ?, ?, ?);";
    private final String READ = "SELECT\n" +
            "  r.id,\n" +
            "  r.free,\n" +
            "  r.expiration_date,\n" +
            "  r.need_extension,\n" +
            "  u.id    AS user_id,\n" +
            "  u.login,\n" +
            "  m.id    AS medication_id,\n" +
            "  m.title AS medication\n" +
            "FROM recipe r INNER JOIN user u ON r.User_id = u.id\n" +
            "  INNER JOIN medication m ON r.Medication_id = m.id\n" +
            "  INNER JOIN status s ON u.status_id = s.id";
    private final String UPDATE = "UPDATE recipe\n" +
            "SET User_id = ?, Medication_id = ?, free = ?, expiration_date = ?, " +
            "need_extension = ? WHERE id = ?;";
    private final String GET_RECIPES_BY_USER = "SELECT\n" +
            "  r.id,\n" +
            "  r.free,\n" +
            "  r.expiration_date,\n" +
            "  r.need_extension,\n" +
            "  u.id    AS user_id,\n" +
            "  u.login,\n" +
            "  m.id    AS medication_id,\n" +
            "  m.title AS medication\n" +
            "FROM recipe r INNER JOIN user u ON r.User_id = u.id\n" +
            "  INNER JOIN medication m ON r.Medication_id = m.id\n" +
            "  INNER JOIN status s ON u.status_id = s.id\n" +
            "  WHERE u.login = ?";
    private final String DELETE = "";
    private ConnectionManager cm = null;
    private Connection connection = null;
    private PreparedStatement preparedStatement;

    private DaoRecipes() {
    }

    public static DaoRecipes getInstance() {
        return instance;
    }

    @Override
    public void create(Recipe entity) throws DaoException {
        try {
            cm = ConnectionManager.getInstance();
            connection = cm.getConnection();
            preparedStatement = connection.prepareStatement(INSERT);
            preparedStatement.setLong(1, entity.getUser().getId());
            preparedStatement.setLong(2, entity.getMedication().getId());
            preparedStatement.setBoolean(3, entity.isFreeOfCharge());
            preparedStatement.setTimestamp(4, entity.getExpirationDate());
            preparedStatement.setBoolean(5, entity.isNeedExtension());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Can't execute prepared statement.", e);
        } catch (ConnectionException e) {
            throw new DaoException("Can't get connection to the database", e);
        } finally {
            ConnectionManager.closeConnectionAndPreparedStatement(connection, preparedStatement);
        }
    }

    @Override
    public List<Recipe> readAll() throws DaoException {
        List<Recipe> list = null;
        try {
            cm = ConnectionManager.getInstance();
            connection = cm.getConnection();
            preparedStatement = connection.prepareStatement(READ);
            ResultSet rs = preparedStatement.executeQuery();
            list = new ArrayList<>();
            while (rs.next()) {
                Recipe recipe = new Recipe();
                recipe.setId(rs.getLong(1));
                recipe.setFreeOfCharge(rs.getByte(2));
                recipe.setExpirationDate(rs.getTimestamp(3));
                recipe.setNeedExtension(rs.getByte(4));
                User user = new User();
                user.setId(rs.getLong(5));
                user.setLogin(rs.getString(6));
                recipe.setUser(user);
                Medication medication = new Medication();
                medication.setId(rs.getLong(7));
                medication.setTitle(rs.getString(8));
                recipe.setMedication(medication);
                list.add(recipe);
            }
        } catch (SQLException e) {
            throw new DaoException("Can't execute prepared statement.", e);
        } catch (ConnectionException e) {
            throw new DaoException("Can't get connection to the database", e);
        } finally {
            ConnectionManager.closeConnectionAndPreparedStatement(connection, preparedStatement);
        }
        return list;
    }

    @Override
    public void update(Recipe entity) throws DaoException {
        try {
            cm = ConnectionManager.getInstance();
            connection = cm.getConnection();
            preparedStatement = connection.prepareStatement(UPDATE);
            preparedStatement.setLong(1, entity.getUser().getId());
            preparedStatement.setLong(2, entity.getMedication().getId());
            preparedStatement.setBoolean(3, entity.isFreeOfCharge());
            preparedStatement.setTimestamp(4, entity.getExpirationDate());
            preparedStatement.setBoolean(5, entity.isNeedExtension());
            preparedStatement.setLong(6, entity.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Can't execute prepared statement.", e);
        } catch (ConnectionException e) {
            throw new DaoException("Can't get connection to the database", e);
        } finally {
            ConnectionManager.closeConnectionAndPreparedStatement(connection, preparedStatement);
        }
    }

    @Override
    public void delete(Recipe entity) throws DaoException {
        try {
            cm = ConnectionManager.getInstance();
            connection = cm.getConnection();
            preparedStatement = connection.prepareStatement(DELETE);
            preparedStatement.setLong(1, entity.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Can't execute prepared statement.", e);
        } catch (ConnectionException e) {
            throw new DaoException("Can't get connection to the database", e);
        } finally {
            ConnectionManager.closeConnectionAndPreparedStatement(connection, preparedStatement);
        }
    }

    public List<Recipe> getRecipesByUser(String userLogin) throws DaoException {
        List<Recipe> list = null;
        try {
            cm = ConnectionManager.getInstance();
            connection = cm.getConnection();
            preparedStatement = connection.prepareStatement(GET_RECIPES_BY_USER);
            preparedStatement.setString(1, userLogin);
            ResultSet rs = preparedStatement.executeQuery();
            list = new ArrayList<>();
            while (rs.next()) {
                Recipe recipe = new Recipe();
                recipe.setId(rs.getLong(1));
                recipe.setFreeOfCharge(rs.getByte(2));
                recipe.setExpirationDate(rs.getTimestamp(3));
                recipe.setNeedExtension(rs.getByte(4));
                User user = new User();
                user.setId(rs.getLong(5));
                user.setLogin(rs.getString(6));
                recipe.setUser(user);
                Medication medication = new Medication();
                medication.setId(rs.getLong(7));
                medication.setTitle(rs.getString(8));
                recipe.setMedication(medication);
                list.add(recipe);
            }
        } catch (SQLException e) {
            throw new DaoException("Can't execute prepared statement.", e);
        } catch (ConnectionException e) {
            throw new DaoException("Can't get connection to the database", e);
        } finally {
            ConnectionManager.closeConnectionAndPreparedStatement(connection, preparedStatement);
        }
        return list;
    }
}
