package by.epam.pharmacy.dao.impl;

import by.epam.pharmacy.dao.ConnectionException;
import by.epam.pharmacy.dao.ConnectionManager;
import by.epam.pharmacy.dao.CrudDao;
import by.epam.pharmacy.dao.DaoException;
import by.epam.pharmacy.entity.Country;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by a.gladkikh on 14/09/16.
 */
public class DaoCountries implements CrudDao<Country> {
    private static final Logger LOGGER = Logger.getLogger(DaoCountries.class);
    private static final DaoCountries instance = new DaoCountries();
    private final String READ = "SELECT\n" +
            "  id,\n" +
            "  name\n" +
            "FROM country";
    private ConnectionManager cm = null;
    private Connection connection = null;
    private PreparedStatement preparedStatement;

    private DaoCountries() {
    }

    public static DaoCountries getInstance() {
        return instance;
    }

    @Override
    public void create(Country entity) throws DaoException {
        throw new DaoException("Calling not-supported operation");
    }

    @Override
    public List<Country> readAll() throws DaoException {
        List<Country> list = null;
        try {
            cm = ConnectionManager.getInstance();
            connection = cm.getConnection();
            preparedStatement = connection.prepareStatement(READ);
            ResultSet rs = preparedStatement.executeQuery();
            list = new ArrayList<>();
            while (rs.next()) {
                Country country = new Country();
                country.setId(rs.getLong(1));
                country.setName(rs.getString(2));
                list.add(country);
            }
        } catch (SQLException e) {
            throw new DaoException("Can't execute prepared statement.", e);
        } catch (ConnectionException e) {
            throw new DaoException("Can't get connection to the database", e);
        } finally {
            ConnectionManager.closeConnectionAndPreparedStatement(connection, preparedStatement);
        }
        return list;
    }

    @Override
    public void update(Country entity) throws DaoException {
        throw new DaoException("Calling not-supported operation");
    }

    @Override
    public void delete(Country entity) throws DaoException {
        throw new DaoException("Calling not-supported operation");
    }

}
