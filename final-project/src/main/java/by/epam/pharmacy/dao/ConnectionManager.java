package by.epam.pharmacy.dao;

import org.apache.log4j.Logger;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;

import javax.naming.Context;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by a.gladkikh on 18/09/16.
 */
public class ConnectionManager {
    private static final Logger LOGGER = Logger.getLogger(ConnectionManager.class);
    private static ConnectionManager instance = null;
    private volatile static boolean instanceCreated = false;
    private static ReentrantLock lock = new ReentrantLock();
    private Context initialContext;
    private DataSource dataSource;
    private Connection connection;

    private ConnectionManager() throws ConnectionException {
            PoolProperties p = new PoolProperties();
            p.setUrl("jdbc:mysql://localhost:3306/pharmacy");
            p.setDriverClassName("com.mysql.jdbc.Driver");
            p.setUsername("root");
            p.setPassword("root");
            p.setJmxEnabled(true);
            p.setTestWhileIdle(false);
            p.setTestOnBorrow(true);
            p.setValidationQuery("SELECT 1");
            p.setTestOnReturn(false);
            p.setValidationInterval(30000);
            p.setTimeBetweenEvictionRunsMillis(30000);
            p.setMaxActive(100);
            p.setInitialSize(10);
            p.setMaxWait(10000);
            p.setRemoveAbandonedTimeout(60);
            p.setMinEvictableIdleTimeMillis(30000);
            p.setMinIdle(10);
            p.setLogAbandoned(true);
            p.setRemoveAbandoned(true);
            p.setJdbcInterceptors(
                    "org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;"+
                            "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");
            dataSource = new DataSource(p);
    }

    public static ConnectionManager getInstance() throws ConnectionException {
        if (!instanceCreated) {
            lock.lock();
            try {
                if (!instanceCreated) {
                    instance = new ConnectionManager();
                    instanceCreated = true;
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    public Connection getConnection() throws ConnectionException {
        try {
            connection = dataSource.getConnection();
        } catch (SQLException e) {
            throw new ConnectionException("Can't get connection to the database", e);
        }
        return connection;
    }

    public static void closeConnectionAndPreparedStatement(Connection connection, PreparedStatement preparedStatement) {
        try {
            if (preparedStatement != null && !preparedStatement.isClosed())
                preparedStatement.close();
            if (connection != null)
                connection.close();
        } catch (SQLException e) {
            LOGGER.error("Can't close PS", e);
        }
    }

}

