package by.epam.pharmacy.dao;

/**
 * Created by a.gladkikh on 08/09/16.
 */
public class ConnectionException extends Exception {
    public ConnectionException(String msg) {
        super(msg);
    }

    public ConnectionException(Throwable cause) {
        super(cause);
    }

    public ConnectionException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
