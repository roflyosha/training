package by.epam.pharmacy.dao.impl;

import by.epam.pharmacy.dao.ConnectionException;
import by.epam.pharmacy.dao.ConnectionManager;
import by.epam.pharmacy.dao.CrudDao;
import by.epam.pharmacy.dao.DaoException;
import by.epam.pharmacy.entity.Country;
import by.epam.pharmacy.entity.Manufacturer;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by a.gladkikh on 27/09/16.
 */
public class DaoManufacturers implements CrudDao<Manufacturer> {
    private static final Logger LOGGER = Logger.getLogger(DaoManufacturers.class);
    private static final DaoManufacturers instance = new DaoManufacturers();
    private final String INSERT = "INSERT INTO manufacturer (name, country_id) VALUES (?, ?);";
    private final String READ = "SELECT\n" +
            "  m.id,\n" +
            "  m.name,\n" +
            "  c.id   AS country_id,\n" +
            "  c.name AS country\n" +
            "FROM manufacturer m INNER JOIN country c ON m.country_id = c.id";
    private final String UPDATE = "UPDATE manufacturer\n" +
            "SET name = ?, country_id = ?\n" +
            "WHERE id = ?;";
    private final String DELETE = "DELETE FROM manufacturer\n" +
            "WHERE id = ?;";
    private ConnectionManager cm = null;
    private Connection connection = null;
    private PreparedStatement preparedStatement;

    private DaoManufacturers() {
    }

    public static DaoManufacturers getInstance() {
        return instance;
    }

    @Override
    public void create(Manufacturer entity) throws DaoException {
        try {
            cm = ConnectionManager.getInstance();
            connection = cm.getConnection();
            preparedStatement = connection.prepareStatement(INSERT);
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setLong(2, entity.getCountry().getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Can't execute prepared statement.", e);
        } catch (ConnectionException e) {
            throw new DaoException("Can't get connection to the database", e);
        } finally {
            ConnectionManager.closeConnectionAndPreparedStatement(connection, preparedStatement);
        }
    }

    @Override
    public List<Manufacturer> readAll() throws DaoException {
        List<Manufacturer> list = null;
        try {
            cm = ConnectionManager.getInstance();
            connection = cm.getConnection();
            preparedStatement = connection.prepareStatement(READ);
            ResultSet rs = preparedStatement.executeQuery();
            list = new ArrayList<>();
            while (rs.next()) {
                Manufacturer manufacturer = new Manufacturer();
                manufacturer.setId(rs.getLong(1));
                manufacturer.setName(rs.getString(2));
                Country country = new Country();
                country.setId(rs.getLong(3));
                country.setName(rs.getString(4));
                manufacturer.setCountry(country);
                list.add(manufacturer);
            }
        } catch (SQLException e) {
            throw new DaoException("Can't execute prepared statement.", e);
        } catch (ConnectionException e) {
            throw new DaoException("Can't get connection to the database", e);
        } finally {
            ConnectionManager.closeConnectionAndPreparedStatement(connection, preparedStatement);
        }
        return list;
    }

    @Override
    public void update(Manufacturer entity) throws DaoException {
        try {
            cm = ConnectionManager.getInstance();
            connection = cm.getConnection();
            preparedStatement = connection.prepareStatement(UPDATE);
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setLong(2, entity.getCountry().getId());
            preparedStatement.setLong(3, entity.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Can't execute prepared statement.", e);
        } catch (ConnectionException e) {
            throw new DaoException("Can't get connection to the database", e);
        } finally {
            ConnectionManager.closeConnectionAndPreparedStatement(connection, preparedStatement);
        }
    }

    @Override
    public void delete(Manufacturer entity) throws DaoException {
        try {
            cm = ConnectionManager.getInstance();
            connection = cm.getConnection();
            preparedStatement = connection.prepareStatement(DELETE);
            preparedStatement.setLong(1, entity.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Can't execute prepared statement.", e);
        } catch (ConnectionException e) {
            throw new DaoException("Can't get connection to the database", e);
        } finally {
            ConnectionManager.closeConnectionAndPreparedStatement(connection, preparedStatement);
        }
    }

}
