package by.epam.pharmacy.dao;

import java.util.List;

/**
 * Created by a.gladkikh on 08/09/16.
 */
public interface CrudDao<T> {
    void create(T entity) throws DaoException;

    List<T> readAll() throws DaoException;

    void update(T entity) throws DaoException;

    void delete(T entity) throws DaoException;

}
