package by.epam.pharmacy.dao.impl;

import by.epam.pharmacy.dao.ConnectionException;
import by.epam.pharmacy.dao.ConnectionManager;
import by.epam.pharmacy.dao.CrudDao;
import by.epam.pharmacy.dao.DaoException;
import by.epam.pharmacy.entity.Medication;
import by.epam.pharmacy.entity.Sale;
import by.epam.pharmacy.entity.User;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by a.gladkikh on 03/10/16.
 */
public class DaoSales implements CrudDao<Sale> {
    private static final Logger LOGGER = Logger.getLogger(DaoSales.class);
    private static final DaoSales instance = new DaoSales();
    private final String INSERT = "INSERT INTO sale " +
            "(Medication_id, User_id, quantity, phase, date_of_sale)\n" +
            "VALUES (?, ?, ?, ?, ?);";
    private final String READ = "SELECT\n" +
            "  s.id,\n" +
            "  m.id AS medication_id,\n" +
            "  m.title,\n" +
            "  u.id AS user_id,\n" +
            "  u.login,\n" +
            "  s.quantity,\n" +
            "  s.phase,\n" +
            "  s.date_of_sale\n" +
            "FROM sale s INNER JOIN medication m ON s.Medication_id = m.id\n" +
            "  INNER JOIN user u ON s.User_id = u.id;";
    private final String UPDATE = "UPDATE sale " +
            "SET Medication_id = ?, User_id = ?, quantity = ?, phase = ?, date_of_sale = ? " +
            "WHERE id = ?;";
    private final String DELETE = "DELETE FROM sale WHERE id=?";
    private final String GET_SALES_BY_USER = "SELECT\n" +
            "  s.id,\n" +
            "  m.id AS medication_id,\n" +
            "  m.title,\n" +
            "  u.id AS user_id,\n" +
            "  u.login,\n" +
            "  s.quantity,\n" +
            "  s.phase,\n" +
            "  s.date_of_sale\n" +
            "FROM sale s INNER JOIN medication m ON s.Medication_id = m.id\n" +
            "  INNER JOIN user u ON s.User_id = u.id\n" +
            "  WHERE u.id = ?";
    private ConnectionManager cm = null;
    private Connection connection = null;
    private PreparedStatement preparedStatement;

    private DaoSales() {
    }

    public static DaoSales getInstance() {
        return instance;
    }

    @Override
    public void create(Sale entity) throws DaoException {
        try {
            cm = ConnectionManager.getInstance();
            connection = cm.getConnection();
            preparedStatement = connection.prepareStatement(INSERT);
            preparedStatement.setLong(1, entity.getMedication().getId());
            preparedStatement.setLong(2, entity.getUser().getId());
            preparedStatement.setInt(3, entity.getQuantity());
            preparedStatement.setString(4, entity.getPhase());
            preparedStatement.setDate(5, entity.getDateOfSale());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Can't execute prepared statement.", e);
        } catch (ConnectionException e) {
            throw new DaoException("Can't get connection to the database", e);
        } finally {
            ConnectionManager.closeConnectionAndPreparedStatement(connection, preparedStatement);
        }
    }

    @Override
    public List<Sale> readAll() throws DaoException {
        List<Sale> list = null;
        try {
            cm = ConnectionManager.getInstance();
            connection = cm.getConnection();
            preparedStatement = connection.prepareStatement(READ);
            ResultSet rs = preparedStatement.executeQuery();
            list = new ArrayList<>();
            while (rs.next()) {
                Sale sale = new Sale();
                sale.setId(rs.getLong(1));
                Medication medication = new Medication();
                medication.setId(rs.getLong(2));
                medication.setTitle(rs.getString(3));
                sale.setMedication(medication);
                User user = new User();
                user.setId(rs.getLong(4));
                user.setLogin(rs.getString(5));
                sale.setUser(user);
                sale.setQuantity(rs.getInt(6));
                sale.setPhase(rs.getString(7));
                sale.setDateOfSale(rs.getDate(8));
                list.add(sale);
            }
        } catch (SQLException e) {
            throw new DaoException("Can't execute prepared statement.", e);
        } catch (ConnectionException e) {
            throw new DaoException("Can't get connection to the database", e);
        } finally {
            ConnectionManager.closeConnectionAndPreparedStatement(connection, preparedStatement);
        }
        return list;
    }

    @Override
    public void update(Sale entity) throws DaoException {
        try {
            cm = ConnectionManager.getInstance();
            connection = cm.getConnection();
            preparedStatement = connection.prepareStatement(UPDATE);
            preparedStatement.setLong(1, entity.getMedication().getId());
            preparedStatement.setLong(2, entity.getUser().getId());
            preparedStatement.setInt(3, entity.getQuantity());
            preparedStatement.setString(4, entity.getPhase());
            preparedStatement.setDate(5, entity.getDateOfSale());
            preparedStatement.setLong(6, entity.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Can't execute prepared statement.", e);
        } catch (ConnectionException e) {
            throw new DaoException("Can't get connection to the database", e);
        } finally {
            ConnectionManager.closeConnectionAndPreparedStatement(connection, preparedStatement);
        }
    }

    @Override
    public void delete(Sale entity) throws DaoException {
        try {
            cm = ConnectionManager.getInstance();
            connection = cm.getConnection();
            preparedStatement = connection.prepareStatement(DELETE);
            preparedStatement.setLong(1, entity.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Can't execute prepared statement.", e);
        } catch (ConnectionException e) {
            throw new DaoException("Can't get connection to the database", e);
        } finally {
            ConnectionManager.closeConnectionAndPreparedStatement(connection, preparedStatement);
        }
    }

    public List<Sale> getSalesByUser(Long userId) throws DaoException {
        List<Sale> list = null;
        try {
            cm = ConnectionManager.getInstance();
            connection = cm.getConnection();
            preparedStatement = connection.prepareStatement(GET_SALES_BY_USER);
            preparedStatement.setLong(1, userId);
            ResultSet rs = preparedStatement.executeQuery();
            list = new ArrayList<>();
            while (rs.next()) {
                Sale sale = new Sale();
                sale.setId(rs.getLong(1));
                Medication medication = new Medication();
                medication.setId(rs.getLong(2));
                medication.setTitle(rs.getString(3));
                sale.setMedication(medication);
                User user = new User();
                user.setId(rs.getLong(4));
                user.setLogin(rs.getString(5));
                sale.setUser(user);
                sale.setQuantity(rs.getInt(6));
                sale.setPhase(rs.getString(7));
                sale.setDateOfSale(rs.getDate(8));
                list.add(sale);
            }
        } catch (SQLException e) {
            throw new DaoException("Can't execute prepared statement.", e);
        } catch (ConnectionException e) {
            throw new DaoException("Can't get connection to the database", e);
        } finally {
            ConnectionManager.closeConnectionAndPreparedStatement(connection, preparedStatement);
        }
        return list;
    }
}
