package by.epam.pharmacy.dao;

import by.epam.pharmacy.dao.impl.*;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by a.gladkikh on 14/09/16.
 */
public class DaoFactoryImpl implements DaoFactory {
    private static final DaoFactoryImpl instance = new DaoFactoryImpl();
    private static final Logger LOGGER = Logger.getLogger(DaoFactoryImpl.class);

    private Map<DaoType, CrudDao> mapDao = new HashMap<>();

    private DaoFactoryImpl() {
        mapDao.put(DaoType.CATEGORIES, DaoCategories.getInstance());
        mapDao.put(DaoType.COUNTRIES, DaoCountries.getInstance());
        mapDao.put(DaoType.MANUFACTURERS, DaoManufacturers.getInstance());
        mapDao.put(DaoType.MEDICATIONS, DaoMedications.getInstance());
        mapDao.put(DaoType.RECIPES, DaoRecipes.getInstance());
        mapDao.put(DaoType.SALES, DaoSales.getInstance());
        mapDao.put(DaoType.STATUSES, DaoStatuses.getInstance());
        mapDao.put(DaoType.USERS, DaoUsers.getInstance());
        mapDao.put(DaoType.APPLICATIONS, DaoApplications.getInstance());
    }

    public static DaoFactoryImpl getInstance() { return instance; }

    public CrudDao getDAO(String name) {
        DaoType type = null;
        try {
            type = DaoType.valueOf(name.toUpperCase());
        } catch (IllegalArgumentException e) {
            LOGGER.error("Enum constant with name " + name + " doesn't exists.");
        }
        return mapDao.get(type);
    }

    public enum DaoType {
        CATEGORIES, COUNTRIES, MANUFACTURERS, MEDICATIONS, RECIPES, SALES, STATUSES, USERS, APPLICATIONS
    }
}
