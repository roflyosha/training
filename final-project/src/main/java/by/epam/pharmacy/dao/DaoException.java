package by.epam.pharmacy.dao;

/**
 * Created by a.gladkikh on 08/09/16.
 */
public class DaoException extends ConnectionException {
    public DaoException(String msg) {
        super(msg);
    }

    public DaoException(Throwable cause) {
        super(cause);
    }

    public DaoException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
