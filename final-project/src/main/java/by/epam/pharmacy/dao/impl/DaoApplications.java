package by.epam.pharmacy.dao.impl;

import by.epam.pharmacy.dao.ConnectionException;
import by.epam.pharmacy.dao.ConnectionManager;
import by.epam.pharmacy.dao.CrudDao;
import by.epam.pharmacy.dao.DaoException;
import by.epam.pharmacy.entity.Application;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by a.gladkikh on 03/10/16.
 */
public class DaoApplications implements CrudDao<Application> {
    private static final Logger LOGGER = Logger.getLogger(DaoApplications.class);
    private static final DaoApplications instance = new DaoApplications();
    private final String READ = "SELECT\n" +
            "  id,\n" +
            "  name\n" +
            "FROM application";
    private ConnectionManager cm = null;
    private Connection connection = null;
    private PreparedStatement preparedStatement;

    private DaoApplications() {
    }

    public static DaoApplications getInstance() {
        return instance;
    }

    @Override
    public void create(Application entity) throws DaoException {
        throw new DaoException("Calling not-supported operation");
    }

    @Override
    public List<Application> readAll() throws DaoException {
        List<Application> list = null;
        try {
            cm = ConnectionManager.getInstance();
            connection = cm.getConnection();
            preparedStatement = connection.prepareStatement(READ);
            ResultSet rs = preparedStatement.executeQuery();
            list = new ArrayList<>();
            while (rs.next()) {
                Application application = new Application();
                application.setId(rs.getLong(1));
                application.setName(rs.getString(2));
                list.add(application);
            }
        } catch (SQLException e) {
            throw new DaoException("Can't execute prepared statement.", e);
        } catch (ConnectionException e) {
            throw new DaoException("Can't get connection to the database", e);
        } finally {
            ConnectionManager.closeConnectionAndPreparedStatement(connection, preparedStatement);
        }
        return null;
    }

    @Override
    public void update(Application entity) throws DaoException {
        throw new DaoException("Calling not-supported operation");
    }

    @Override
    public void delete(Application entity) throws DaoException {
        throw new DaoException("Calling not-supported operation");
    }
}
