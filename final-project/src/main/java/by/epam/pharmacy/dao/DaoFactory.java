package by.epam.pharmacy.dao;

/**
 * Created by a.gladkikh on 12/09/16.
 */
public interface DaoFactory {
    CrudDao getDAO(String name);
}
