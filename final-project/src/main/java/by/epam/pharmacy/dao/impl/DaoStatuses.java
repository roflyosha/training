package by.epam.pharmacy.dao.impl;

import by.epam.pharmacy.dao.ConnectionException;
import by.epam.pharmacy.dao.ConnectionManager;
import by.epam.pharmacy.dao.CrudDao;
import by.epam.pharmacy.dao.DaoException;
import by.epam.pharmacy.entity.Status;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by a.gladkikh on 03/10/16.
 */
public class DaoStatuses implements CrudDao<Status> {
    private static final Logger LOGGER = Logger.getLogger(DaoStatuses.class);
    private static final DaoStatuses instance = new DaoStatuses();
    private final String READ = "SELECT\n" +
            "  id,\n" +
            "  name\n" +
            "FROM status;";
    private ConnectionManager cm = null;
    private Connection connection = null;
    private PreparedStatement preparedStatement;

    private DaoStatuses() {
    }

    public static DaoStatuses getInstance() {
        return instance;
    }

    @Override
    public void create(Status entity) throws DaoException {
        throw new DaoException("Calling not-supported operation");
    }

    @Override
    public List<Status> readAll() throws DaoException {
        List<Status> list = null;
        try {
            cm = ConnectionManager.getInstance();
            connection = cm.getConnection();
            preparedStatement = connection.prepareStatement(READ);
            ResultSet rs = preparedStatement.executeQuery();
            list = new ArrayList<>();
            while (rs.next()) {
                Status status = new Status();
                status.setId(rs.getLong(1));
                status.setName(rs.getString(2));
                list.add(status);
            }
        } catch (SQLException e) {
            throw new DaoException("Can't execute prepared statement.", e);
        } catch (ConnectionException e) {
            throw new DaoException("Can't get connection to the database", e);
        } finally {
            ConnectionManager.closeConnectionAndPreparedStatement(connection, preparedStatement);
        }
        return list;
    }

    @Override
    public void update(Status entity) throws DaoException {
        throw new DaoException("Calling not-supported operation");
    }

    @Override
    public void delete(Status entity) throws DaoException {
        throw new DaoException("Calling not-supported operation");
    }
}
