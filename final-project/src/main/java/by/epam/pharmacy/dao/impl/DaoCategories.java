package by.epam.pharmacy.dao.impl;

import by.epam.pharmacy.dao.ConnectionException;
import by.epam.pharmacy.dao.ConnectionManager;
import by.epam.pharmacy.dao.CrudDao;
import by.epam.pharmacy.dao.DaoException;
import by.epam.pharmacy.entity.Category;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by a.gladkikh on 14/09/16.
 */
public class DaoCategories implements CrudDao<Category> {
    public static final DaoCategories instance = new DaoCategories();
    private static final Logger LOGGER = Logger.getLogger(DaoCategories.class);
    private final String READ = "SELECT\n" +
            "  id,\n" +
            "  name\n" +
            "FROM category;";
    private final String INSERT = "INSERT INTO category (name) VALUES (?);";
    private final String UPDATE = "UPDATE category\n" +
            "SET name = ?\n" +
            "WHERE id = ?;";
    private final String DELETE = "DELETE FROM category\n" +
            "WHERE id = ?;";
    private ConnectionManager cm = null;
    private Connection connection = null;
    private PreparedStatement preparedStatement;

    private DaoCategories() {
    }

    public static DaoCategories getInstance() {
        return instance;
    }

    @Override
    public void create(Category entity) throws DaoException {
        try {
            cm = ConnectionManager.getInstance();
            connection = cm.getConnection();
            preparedStatement = connection.prepareStatement(INSERT);
            preparedStatement.setString(1, entity.getName());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Can't execute prepared statement.", e);
        } catch (ConnectionException e) {
            throw new DaoException("Can't get connection to the database", e);
        } finally {
            ConnectionManager.closeConnectionAndPreparedStatement(connection, preparedStatement);
        }
    }


    @Override
    public List<Category> readAll() throws DaoException {
        List<Category> list = null;
        try {
            cm = ConnectionManager.getInstance();
            connection = cm.getConnection();
            preparedStatement = connection.prepareStatement(READ);
            ResultSet rs = preparedStatement.executeQuery();
            list = new ArrayList<>();
            while (rs.next()) {
                Category category = new Category();
                category.setId(rs.getLong(1));
                category.setName(rs.getString(2));
                list.add(category);
            }
        } catch (SQLException e) {
            throw new DaoException("Can't execute prepared statement.", e);
        } catch (ConnectionException e) {
            throw new DaoException("Can't get connection to the database", e);
        } finally {
            ConnectionManager.closeConnectionAndPreparedStatement(connection, preparedStatement);
        }
        return list;
    }

    @Override
    public void update(Category entity) throws DaoException {
        try {
            cm = ConnectionManager.getInstance();
            connection = cm.getConnection();
            preparedStatement = connection.prepareStatement(UPDATE);
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setLong(2, entity.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Can't execute prepared statement.", e);
        } catch (ConnectionException e) {
            throw new DaoException("Can't get connection to the database", e);
        } finally {
            ConnectionManager.closeConnectionAndPreparedStatement(connection, preparedStatement);
        }
    }


    @Override
    public void delete(Category entity) throws DaoException {
        try {
            cm = ConnectionManager.getInstance();
            connection = cm.getConnection();
            preparedStatement = connection.prepareStatement(DELETE);
            preparedStatement.setLong(1, entity.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Can't execute prepared statement.", e);
        } catch (ConnectionException e) {
            throw new DaoException("Can't get connection to the database", e);
        } finally {
            ConnectionManager.closeConnectionAndPreparedStatement(connection, preparedStatement);
        }
    }

}
