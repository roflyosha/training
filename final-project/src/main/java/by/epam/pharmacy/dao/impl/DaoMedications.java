package by.epam.pharmacy.dao.impl;

import by.epam.pharmacy.dao.ConnectionException;
import by.epam.pharmacy.dao.ConnectionManager;
import by.epam.pharmacy.dao.CrudDao;
import by.epam.pharmacy.dao.DaoException;
import by.epam.pharmacy.entity.*;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by a.gladkikh on 29/09/16.
 */
public class DaoMedications implements CrudDao<Medication> {
    private static final Logger LOGGER = Logger.getLogger(DaoMedications.class);
    private static final DaoMedications instance = new DaoMedications();
    private final String INSERT = "INSERT INTO medication (title, instructions, need_recipe, " +
            "quantity, unit_price, Category_id, Manufacturer_id, application_id)\n" +
            "VALUES (?, ?, ?, ?, ?, ?, ?, ?);";
    private final String READ = "SELECT\n" +
            "  m.id,\n" +
            "  m.title,\n" +
            "  m.instructions,\n" +
            "  m.need_recipe,\n" +
            "  m.quantity,\n" +
            "  m.unit_price,\n" +
            "  cnt.id   AS country_id,\n" +
            "  cnt.name AS country,\n" +
            "  man.id   AS manufacturer_id,\n" +
            "  man.name AS manufacturer,\n" +
            "  c.id     AS category_id,\n" +
            "  c.name   AS category,\n" +
            "  a.id     AS application_id,\n" +
            "  a.name   AS application_form\n" +
            "FROM medication m INNER JOIN manufacturer man\n" +
            "    ON m.Manufacturer_id = man.id\n" +
            "  INNER JOIN country cnt\n" +
            "    ON man.country_id = cnt.id\n" +
            "  INNER JOIN category c\n" +
            "    ON m.Category_id = c.id\n" +
            "  INNER JOIN application a\n" +
            "    ON m.application_id = a.id";
    private final String UPDATE = "UPDATE medication\n" +
            "SET title = ?, instructions = ?, need_recipe = ?, quantity = ?, unit_price = ?, " +
            "Category_id = ?, Manufacturer_id = ?, application_id = ?\n" +
            "WHERE id = ?;";
    private final String DELETE = "DELETE FROM medication\n" +
            "WHERE id = ?;";
    private final String GET_MEDICATIONS_BY_CATEGORY = "SELECT\n" +
            "  m.id,\n" +
            "  m.title,\n" +
            "  m.instructions,\n" +
            "  m.need_recipe,\n" +
            "  m.quantity,\n" +
            "  m.unit_price,\n" +
            "  man.id   AS manufacturer_id,\n" +
            "  man.name AS manufacturer,\n" +
            "  cnt.id,\n" +
            "  cnt.name AS country,\n" +
            "  c.id,\n" +
            "  c.name   AS category,\n" +
            "  a.id,\n" +
            "  a.name   AS application_form\n" +
            "FROM medication m INNER JOIN manufacturer man\n" +
            "    ON m.Manufacturer_id = man.id\n" +
            "  INNER JOIN country cnt\n" +
            "    ON man.country_id = cnt.id\n" +
            "  INNER JOIN category c\n" +
            "    ON m.Category_id = c.id\n" +
            "  INNER JOIN application a\n" +
            "    ON m.application_id = a.id\n" +
            "WHERE c.id = ?;";
    private ConnectionManager cm = null;
    private Connection connection = null;
    private PreparedStatement preparedStatement;

    private DaoMedications() {
    }

    public static DaoMedications getInstance() {
        return instance;
    }

    @Override
    public void create(Medication entity) throws DaoException {
        try {
            cm = ConnectionManager.getInstance();
            connection = cm.getConnection();
            preparedStatement = connection.prepareStatement(INSERT);
            preparedStatement.setString(1, entity.getTitle());
            preparedStatement.setString(2, entity.getInstructions());
            preparedStatement.setBoolean(3, entity.isNeedRecipe());
            preparedStatement.setFloat(4, entity.getQuantity());
            preparedStatement.setFloat(5, entity.getUnitPrice());
            preparedStatement.setLong(6, entity.getCategory().getId());
            preparedStatement.setLong(7, entity.getManufacturer().getId());
            preparedStatement.setLong(8, entity.getApplication().getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Can't execute prepared statement.", e);
        } catch (ConnectionException e) {
            throw new DaoException("Can't get connection to the database", e);
        } finally {
            ConnectionManager.closeConnectionAndPreparedStatement(connection, preparedStatement);
        }
    }

    @Override
    public List<Medication> readAll() throws DaoException {
        List<Medication> list = null;
        try {
            cm = ConnectionManager.getInstance();
            connection = cm.getConnection();
            preparedStatement = connection.prepareStatement(READ);
            ResultSet rs = preparedStatement.executeQuery();
            list = new ArrayList<>();
            while (rs.next()) {
                Medication medication = new Medication();
                medication.setId(rs.getLong(1));
                medication.setTitle(rs.getString(2));
                medication.setInstructions(rs.getString(3));
                medication.setNeedRecipe(rs.getByte(4));
                medication.setQuantity(rs.getFloat(5));
                medication.setUnitPrice(rs.getFloat(6));
                Country country = new Country();
                country.setId(rs.getLong(7));
                country.setName(rs.getString(8));
                Manufacturer manufacturer = new Manufacturer();
                manufacturer.setId(rs.getLong(9));
                manufacturer.setName(rs.getString(10));
                manufacturer.setCountry(country);
                Category category = new Category();
                category.setId(rs.getLong(11));
                category.setName(rs.getString(12));
                Application application = new Application();
                application.setId(rs.getLong(13));
                application.setName(rs.getString(14));
                medication.setCategory(category);
                medication.setManufacturer(manufacturer);
                medication.setApplication(application);
                list.add(medication);
            }
        } catch (SQLException e) {
            throw new DaoException("Can't execute prepared statement.", e);
        } catch (ConnectionException e) {
            throw new DaoException("Can't get connection to the database", e);
        } finally {
            ConnectionManager.closeConnectionAndPreparedStatement(connection, preparedStatement);
        }
        return list;
    }

    @Override
    public void update(Medication entity) throws DaoException {
        try {
            cm = ConnectionManager.getInstance();
            connection = cm.getConnection();
            preparedStatement = connection.prepareStatement(UPDATE);
            preparedStatement.setString(1, entity.getTitle());
            preparedStatement.setString(2, entity.getInstructions());
            preparedStatement.setBoolean(3, entity.isNeedRecipe());
            preparedStatement.setFloat(4, entity.getQuantity());
            preparedStatement.setFloat(5, entity.getUnitPrice());
            preparedStatement.setLong(6, entity.getCategory().getId());
            preparedStatement.setLong(7, entity.getManufacturer().getId());
            preparedStatement.setLong(8, entity.getApplication().getId());
            preparedStatement.setLong(9, entity.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Can't execute prepared statement.", e);
        } catch (ConnectionException e) {
            throw new DaoException("Can't get connection to the database", e);
        } finally {
            ConnectionManager.closeConnectionAndPreparedStatement(connection, preparedStatement);
        }
    }

    @Override
    public void delete(Medication entity) throws DaoException {
        try {
            cm = ConnectionManager.getInstance();
            connection = cm.getConnection();
            preparedStatement = connection.prepareStatement(DELETE);
            preparedStatement.setLong(1, entity.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException("Can't execute prepared statement.", e);
        } catch (ConnectionException e) {
            throw new DaoException("Can't get connection to the database", e);
        } finally {
            ConnectionManager.closeConnectionAndPreparedStatement(connection, preparedStatement);
        }
    }

    public List<Medication> getMedicationsByCategory(Long categoryId) throws DaoException {
        List<Medication> list = null;
        try {
            cm = ConnectionManager.getInstance();
            connection = cm.getConnection();
            if (categoryId == 0L) {
                preparedStatement = connection.prepareStatement(READ);
            } else {
                preparedStatement = connection.prepareStatement(GET_MEDICATIONS_BY_CATEGORY);
                preparedStatement.setLong(1, categoryId);
            }
            ResultSet rs = preparedStatement.executeQuery();
            list = new ArrayList<>();
            while (rs.next()) {
                Medication medication = new Medication();
                medication.setId(rs.getLong(1));
                medication.setTitle(rs.getString(2));
                medication.setInstructions(rs.getString(3));
                medication.setNeedRecipe(rs.getByte(4));
                medication.setQuantity(rs.getFloat(5));
                medication.setUnitPrice(rs.getFloat(6));
                Country country = new Country();
                country.setId(rs.getLong(7));
                country.setName(rs.getString(8));
                Manufacturer manufacturer = new Manufacturer();
                manufacturer.setId(rs.getLong(9));
                manufacturer.setName(rs.getString(10));
                manufacturer.setCountry(country);
                Category category = new Category();
                category.setId(rs.getLong(11));
                category.setName(rs.getString(12));
                Application application = new Application();
                application.setId(rs.getLong(13));
                application.setName(rs.getString(14));
                medication.setCategory(category);
                medication.setManufacturer(manufacturer);
                medication.setApplication(application);
                list.add(medication);
            }
        } catch (SQLException e) {
            throw new DaoException("Can't execute prepared statement.", e);
        } catch (ConnectionException e) {
            throw new DaoException("Can't get connection to the database", e);
        } finally {
            ConnectionManager.closeConnectionAndPreparedStatement(connection, preparedStatement);
        }
        return list;
    }
}
