package by.epam.pharmacy.controller.filter;

import by.epam.pharmacy.controller.PageHelper;
import by.epam.pharmacy.controller.command.CommandName;
import by.epam.pharmacy.entity.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by a.gladkikh on 12/10/16.
 */
public class AccessFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpSession session = ((HttpServletRequest) request).getSession(false);
        String userStatus = ((User) session.getAttribute("user")).getStatus().getName();
        CommandName command = getCommandName((HttpServletRequest) request);
        boolean accessGranted = checkAccess(userStatus, command);
        if (accessGranted)
            chain.doFilter(request, response);
        else {
            ((HttpServletResponse) response).sendRedirect(PageHelper.getInstance()
                    .getProperty(PageHelper.ACCESS_DENIED_PAGE));
        }
    }

    private boolean checkAccess(String status, CommandName command) {
        boolean access = false;
        UserStatus userStatus = UserStatus.valueOf(status.toUpperCase());
        switch (userStatus) {
            case GUEST:
                switch (command) {
                    case CHANGE_LOCALE:
                    case FILTER_MEDICATIONS:
                    case OPEN_MEDICATION:
                    case REGISTER:
                    case SIGN_IN:
                    case SIGN_OUT:
                    case WRONG_COMMAND:
                        access = true;
                        break;
                    default:
                        access = false;
                        break;
                }
                break;
            case CUSTOMER:
                switch (command) {
                    case CHANGE_LOCALE:
                    case FILTER_MEDICATIONS:
                    case OPEN_MEDICATION:
                    case OPEN_RECIPE:
                    case UPDATE_RECIPE:
                    case OPEN_SALE:
                    case SIGN_IN:
                    case SIGN_OUT:
                    case WRONG_COMMAND:
                        access = true;
                        break;
                    default:
                        access = false;
                        break;
                }
                break;
            case DOCTOR:
                switch (command) {
                    case OPEN_MEDICATION:
                    case FILTER_MEDICATIONS:
                    case LOAD_DATA_TO_EDIT_RECIPE:
                    case ADD_RECIPE:
                    case DELETE_RECIPE:
                    case UPDATE_RECIPE:
                    case OPEN_RECIPE:
                    case CHANGE_LOCALE:
                    case SIGN_IN:
                    case SIGN_OUT:
                    case WRONG_COMMAND:
                        access = true;
                        break;
                    default:
                        access = false;
                        break;
                }
                break;
            case PHARMACIST:
                switch (command) {
                    case OPEN_MEDICATION:
                    case ADD_MEDICATION:
                    case DELETE_MEDICATION:
                    case FILTER_MEDICATIONS:
                    case LOAD_DATA_TO_EDIT_MEDICATION:
                    case UPDATE_MEDICATION:
                    case OPEN_SALE:
                    case CHANGE_LOCALE:
                    case SIGN_IN:
                    case SIGN_OUT:
                    case WRONG_COMMAND:
                        access = true;
                        break;
                    default:
                        access = false;
                        break;
                }
                break;
            case ADMIN:
                switch (command) {
                    case OPEN_USER:
                    case DELETE_USER:
                    case CHANGE_LOCALE:
                    case OPEN_MEDICATION:
                    case FILTER_MEDICATIONS:
                    case SIGN_IN:
                    case SIGN_OUT:
                    case WRONG_COMMAND:
                        access = true;
                        break;
                    default:
                        access = false;
                        break;
                }
                break;
        }
        return access;
    }

    private CommandName getCommandName(HttpServletRequest request) {
        String commandName;
        if (isAjax(request)) {
            commandName = (String) request.getAttribute("command");
        } else {
            commandName = request.getParameter("command");
        }
        CommandName action;
        try {
            action = CommandName.valueOf(commandName.toUpperCase());
        } catch (IllegalArgumentException ex) {
            action = CommandName.WRONG_COMMAND;
        }
        return action;

    }

    private boolean isAjax(HttpServletRequest request) {
        return "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
    }

    private enum UserStatus {
        GUEST, CUSTOMER, DOCTOR, PHARMACIST, ADMIN
    }
}
