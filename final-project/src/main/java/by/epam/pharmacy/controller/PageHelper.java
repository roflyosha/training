package by.epam.pharmacy.controller;

import java.util.ResourceBundle;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by a.gladkikh on 10/10/16.
 */
public class PageHelper {
    public static final String START_PAGE = "START_PAGE";
    public static final String MEDICATIONS_PAGE = "MEDICATIONS_PAGE";
    public static final String RECIPES_PAGE = "RECIPES_PAGE";
    public static final String USERS_PAGE = "USERS_PAGE";
    public static final String SALES_PAGE = "SALES_PAGE";
    public static final String EDIT_MEDICATION_PAGE = "EDIT_MEDICATION_PAGE";
    public static final String EDIT_RECIPE_PAGE = "EDIT_RECIPE_PAGE";
    public static final String ACCESS_DENIED_PAGE = "ACCESS_DENIED_PAGE";
    public static final String ERROR_PAGE = "ERROR_PAGE";
    private static final String BUNDLE_NAME = "pages";
    private static PageHelper instance;
    private static ReentrantLock lock = new ReentrantLock();
    private ResourceBundle bundle;

    private PageHelper() {
        bundle = ResourceBundle.getBundle(BUNDLE_NAME);
    }

    public static PageHelper getInstance() {
        PageHelper local = instance;
        if (local == null) {
            lock.lock();
            try {
                local = instance;
                if (local == null) {
                    instance = local = new PageHelper();
                }
            } finally {
                lock.unlock();
            }
        }
        return local;
    }

    public String getProperty(String key) {
        return (String) bundle.getObject(key);
    }
}
