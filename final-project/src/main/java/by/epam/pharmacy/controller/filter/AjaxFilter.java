package by.epam.pharmacy.controller.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by a.gladkikh on 12/10/16.
 */
public class AjaxFilter implements Filter {
    private static final Logger LOG = Logger.getLogger(AjaxFilter.class);
    private static final String COMMAND = "command";
    private static final String JSON = "json";
    private static final String AJAX_REQUEST = "XMLHttpRequest";
    private static final String REQUEST = "X-Requested-With";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        String commandName;
        HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
        if (isAjax(httpRequest)) {
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(servletRequest.getInputStream()));
                String json = br.readLine();
                ObjectMapper mapper = new ObjectMapper();
                AjaxRequest ajaxRequest = mapper.readValue(json, AjaxRequest.class);
                commandName = ajaxRequest.getCommand();
                servletRequest.setAttribute(JSON, ajaxRequest.getData());
                servletRequest.setAttribute(COMMAND, commandName);
            } catch (IOException ex) {
                LOG.error(ex);
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
    }

    private boolean isAjax(HttpServletRequest request) {
        return AJAX_REQUEST.equals(request.getHeader(REQUEST));
    }

    public static class AjaxRequest {
        private String command;
        private String data;

        public String getCommand() {
            return command;
        }

        public void setCommand(String command) {
            this.command = command;
        }

        public String getData() {
            return data;
        }

        public void setData(String data) {
            this.data = data;
        }
    }
}
