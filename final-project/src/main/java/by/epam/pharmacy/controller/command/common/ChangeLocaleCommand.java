package by.epam.pharmacy.controller.command.common;

import com.fasterxml.jackson.databind.ObjectMapper;
import by.epam.pharmacy.controller.command.ActionCommand;
import by.epam.pharmacy.controller.command.exception.CommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by a.gladkikh on 12/10/16.
 */
public class ChangeLocaleCommand implements ActionCommand {
    private final static String JSON = "json";
    private static final String LOCALE = "locale";

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String json = (String) request.getAttribute(JSON);
        ObjectMapper mapper = new ObjectMapper();
        try {
            String locale = mapper.readValue(json, String.class);
            HttpSession session = request.getSession(true);
            session.setAttribute(LOCALE, locale);
            mapper.writeValue(response.getOutputStream(), 1);
        } catch (IOException e) {
            throw new CommandException(e);
        }
        return null;
    }
}
