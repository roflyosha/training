package by.epam.pharmacy.controller.command.pharmacist;

import com.fasterxml.jackson.databind.ObjectMapper;
import by.epam.pharmacy.controller.PageHelper;
import by.epam.pharmacy.controller.command.ActionCommand;
import by.epam.pharmacy.controller.command.exception.CommandException;
import by.epam.pharmacy.dao.DaoException;
import by.epam.pharmacy.dao.DaoFactoryImpl;
import by.epam.pharmacy.entity.Medication;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by a.gladkikh on 05/10/16.
 */
public class AddMedicationCommand implements ActionCommand {
    private static final String JSON = "json";

    @SuppressWarnings("unchecked")
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String page = PageHelper.getInstance().getProperty(PageHelper.MEDICATIONS_PAGE);
        String json = (String) request.getAttribute(JSON);
        ObjectMapper mapper = new ObjectMapper();
        try {
            Medication medication = mapper.readValue(json, Medication.class);
            DaoFactoryImpl.getInstance().getDAO(DaoFactoryImpl.DaoType.MEDICATIONS.name()).create(medication);
            mapper.writeValue(response.getOutputStream(), -1);
        } catch (IOException | DaoException e) {
            throw new CommandException(e);
        }
        return page;
    }
}
