package by.epam.pharmacy.controller.command.common;

import by.epam.pharmacy.controller.PageHelper;
import by.epam.pharmacy.controller.command.ActionCommand;
import by.epam.pharmacy.controller.command.exception.CommandException;
import by.epam.pharmacy.dao.CrudDao;
import by.epam.pharmacy.dao.DaoException;
import by.epam.pharmacy.dao.DaoFactory;
import by.epam.pharmacy.dao.DaoFactoryImpl;
import by.epam.pharmacy.dao.impl.DaoMedications;
import by.epam.pharmacy.entity.Category;
import by.epam.pharmacy.entity.Medication;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by a.gladkikh on 10/10/16.
 */
public class OpenMedicationCommand implements ActionCommand {
    private static final String MEDICATIONS = "medications";
    private static final String CATEGORIES = "categories";
    private static final Long ANY_CATEGORY_ID = 0L;

    @SuppressWarnings("unchecked")
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String page = PageHelper.getInstance().getProperty(PageHelper.MEDICATIONS_PAGE);
        DaoFactory factory = DaoFactoryImpl.getInstance();
        List<Category> categories;
        try {
            categories = factory.getDAO(DaoFactoryImpl.DaoType.CATEGORIES.name()).readAll();
            request.setAttribute(CATEGORIES, categories);
        } catch (DaoException e) {
            throw new CommandException("Can't load list of categories", e);
        }
        try {
            CrudDao<Medication> dao = factory.getDAO(DaoFactoryImpl.DaoType.MEDICATIONS.name());
            if (dao.getClass() == DaoMedications.class && categories != null && categories.size() != 0) {
                List<Medication> medications = ((DaoMedications) dao).getMedicationsByCategory(ANY_CATEGORY_ID);
                request.setAttribute(MEDICATIONS, medications);
            }
        } catch (DaoException e) {
            throw new CommandException("Can't load list of medications", e);
        }
        return page;
    }
}
