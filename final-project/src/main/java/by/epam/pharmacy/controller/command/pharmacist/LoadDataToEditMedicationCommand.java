package by.epam.pharmacy.controller.command.pharmacist;

import by.epam.pharmacy.controller.PageHelper;
import by.epam.pharmacy.controller.command.ActionCommand;
import by.epam.pharmacy.controller.command.exception.CommandException;
import by.epam.pharmacy.dao.DaoException;
import by.epam.pharmacy.dao.DaoFactory;
import by.epam.pharmacy.dao.DaoFactoryImpl;
import by.epam.pharmacy.entity.Application;
import by.epam.pharmacy.entity.Category;
import by.epam.pharmacy.entity.Manufacturer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by a.gladkikh on 11/10/16.
 */
public class LoadDataToEditMedicationCommand implements ActionCommand {
    private static final String CATEGORIES = "categories";
    private static final String MANUFACTURERS = "manufacturers";
    private static final String APPLICATIONS = "applications";

    @SuppressWarnings("unchecked")
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String page = PageHelper.getInstance().getProperty(PageHelper.EDIT_MEDICATION_PAGE);
        DaoFactory factory = DaoFactoryImpl.getInstance();
        try {
            List<Category> categories = factory.getDAO(DaoFactoryImpl.DaoType.CATEGORIES.name()).readAll();
            request.setAttribute(CATEGORIES, categories);
        } catch (DaoException e) {
            throw new CommandException("Can't load list of categories", e);
        }
        try {
            List<Manufacturer> manufacturers = factory.getDAO(DaoFactoryImpl.DaoType.MANUFACTURERS.name()).readAll();
            request.setAttribute(MANUFACTURERS, manufacturers);
        } catch (DaoException e) {
            throw new CommandException("Can't load list of manufacturers", e);
        }
        try {
            List<Application> applications = factory.getDAO(DaoFactoryImpl.DaoType.APPLICATIONS.name()).readAll();
            request.setAttribute(APPLICATIONS, applications);
        } catch (DaoException e) {
            throw new CommandException("Can't load list of applications", e);
        }
        return page;
    }
}
