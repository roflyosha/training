package by.epam.pharmacy.controller.command.exception;

import by.epam.pharmacy.dao.DaoException;

/**
 * Created by a.gladkikh on 04/10/16.
 */
public class CommandException extends DaoException {
    public CommandException(String msg) {
        super(msg);
    }

    public CommandException(Throwable cause) {
        super(cause);
    }

    public CommandException(String msg, Exception ex) {
        super(msg, ex);
    }
}
