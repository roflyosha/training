package by.epam.pharmacy.controller.command;

import by.epam.pharmacy.controller.command.exception.CommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by a.gladkikh on 01/09/16.
 */
public interface ActionCommand {
    String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException;
}
