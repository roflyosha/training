package by.epam.pharmacy.controller.command.common;

import by.epam.pharmacy.controller.PageHelper;
import by.epam.pharmacy.controller.command.ActionCommand;
import by.epam.pharmacy.controller.command.exception.CommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by a.gladkikh on 01/09/16.
 */
public class WrongCommand implements ActionCommand {

    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        return PageHelper.getInstance().getProperty(PageHelper.ERROR_PAGE);
    }
}
