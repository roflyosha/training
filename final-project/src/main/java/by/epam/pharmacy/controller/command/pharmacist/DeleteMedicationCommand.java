package by.epam.pharmacy.controller.command.pharmacist;

import com.fasterxml.jackson.databind.ObjectMapper;
import by.epam.pharmacy.controller.command.ActionCommand;
import by.epam.pharmacy.controller.command.exception.CommandException;
import by.epam.pharmacy.dao.DaoException;
import by.epam.pharmacy.dao.DaoFactoryImpl;
import by.epam.pharmacy.entity.Medication;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by a.gladkikh on 12/10/16.
 */
public class DeleteMedicationCommand implements ActionCommand {
    private static final String JSON = "json";

    @SuppressWarnings("unchecked")
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String json = (String) request.getAttribute(JSON);
        ObjectMapper mapper = new ObjectMapper();
        try {
            Medication medication = mapper.readValue(json, Medication.class);
            DaoFactoryImpl.getInstance().getDAO(DaoFactoryImpl.DaoType.MEDICATIONS.name()).delete(medication);
            mapper.writeValue(response.getOutputStream(), -1);
        } catch (IOException | DaoException e) {
            throw new CommandException(e);
        }
        return null;
    }
}
