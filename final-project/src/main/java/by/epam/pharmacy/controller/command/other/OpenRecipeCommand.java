package by.epam.pharmacy.controller.command.other;

import by.epam.pharmacy.controller.PageHelper;
import by.epam.pharmacy.controller.command.ActionCommand;
import by.epam.pharmacy.controller.command.exception.CommandException;
import by.epam.pharmacy.dao.CrudDao;
import by.epam.pharmacy.dao.DaoException;
import by.epam.pharmacy.dao.DaoFactory;
import by.epam.pharmacy.dao.DaoFactoryImpl;
import by.epam.pharmacy.dao.impl.DaoRecipes;
import by.epam.pharmacy.entity.Recipe;
import by.epam.pharmacy.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by a.gladkikh on 12/10/16.
 */
public class OpenRecipeCommand implements ActionCommand {
    private static final String USER = "user";
    private static final String RECIPES = "recipes";

    @SuppressWarnings("unchecked")
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String page = PageHelper.getInstance().getProperty(PageHelper.RECIPES_PAGE);
        DaoFactory factory = DaoFactoryImpl.getInstance();
        List<Recipe> recipes;
        HttpSession session = request.getSession(true);
        User user = (User) request.getAttribute(USER);
        if (user.getStatus().getName().compareToIgnoreCase("doctor") == 0) {
            try {
                recipes = factory.getDAO(DaoFactoryImpl.DaoType.RECIPES.name()).readAll();
                request.setAttribute(RECIPES, recipes);
            } catch (DaoException e) {
                throw new CommandException("Can't load list of recipes", e);
            }
        }
        else if (user.getStatus().getName().compareToIgnoreCase("customer") == 0) {
            try {
                CrudDao<Recipe> dao = factory.getDAO(DaoFactoryImpl.DaoType.RECIPES.name());
                recipes = ((DaoRecipes)dao).getRecipesByUser(user.getLogin());
                request.setAttribute(RECIPES, recipes);
            } catch (DaoException e) {
                throw new CommandException("Can't load concrete user's recipes", e);
            }
        }
        return page;
    }
}
