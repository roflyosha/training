package by.epam.pharmacy.controller;

import by.epam.pharmacy.controller.command.ActionCommand;
import by.epam.pharmacy.controller.command.CommandHelper;
import by.epam.pharmacy.controller.command.exception.CommandException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by a.gladkikh on 12/10/16.
 */
@WebServlet("/controller")
public class Controller extends HttpServlet {
    private CommandHelper commandHelper = CommandHelper.getInstance();

    public Controller() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doAction(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doAction(req, resp);
    }

    private void doAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        boolean isAjax = isAjax(request);
        String commandName;
        if (isAjax) {
            commandName = (String) request.getAttribute("command");
        } else {
            commandName = request.getParameter("command");
        }
        ActionCommand command = commandHelper.getCommand(commandName);
        String page;
        try {
            page = command.execute(request, response);
            if (isAjax) {
                return;
            }
        } catch (CommandException e) {
            page = PageHelper.getInstance().getProperty(PageHelper.ERROR_PAGE);
            request.setAttribute("exception", e);
        }
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
        dispatcher.forward(request, response);
    }

    @Override
    public void destroy() {
    }

    private boolean isAjax(HttpServletRequest request) {
        return "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
    }
}
