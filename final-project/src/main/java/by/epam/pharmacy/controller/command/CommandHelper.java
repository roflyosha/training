package by.epam.pharmacy.controller.command;

import by.epam.pharmacy.controller.command.admin.DeleteUserCommand;
import by.epam.pharmacy.controller.command.admin.OpenUserCommand;
import by.epam.pharmacy.controller.command.common.ChangeLocaleCommand;
import by.epam.pharmacy.controller.command.common.FilterMedicationsListCommand;
import by.epam.pharmacy.controller.command.common.OpenMedicationCommand;
import by.epam.pharmacy.controller.command.common.WrongCommand;
import by.epam.pharmacy.controller.command.customer.AddSaleCommand;
import by.epam.pharmacy.controller.command.doctor.AddRecipeCommand;
import by.epam.pharmacy.controller.command.doctor.DeleteRecipeCommand;
import by.epam.pharmacy.controller.command.doctor.LoadDataToEditRecipeCommand;
import by.epam.pharmacy.controller.command.other.UpdateRecipeCommand;
import by.epam.pharmacy.controller.command.guest.RegisterCommand;
import by.epam.pharmacy.controller.command.guest.SignInCommand;
import by.epam.pharmacy.controller.command.other.OpenRecipeCommand;
import by.epam.pharmacy.controller.command.other.OpenSaleCommand;
import by.epam.pharmacy.controller.command.other.SignOutCommand;
import by.epam.pharmacy.controller.command.pharmacist.AddMedicationCommand;
import by.epam.pharmacy.controller.command.pharmacist.DeleteMedicationCommand;
import by.epam.pharmacy.controller.command.pharmacist.LoadDataToEditMedicationCommand;
import by.epam.pharmacy.controller.command.pharmacist.UpdateMedicationCommand;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by a.gladkikh on 12/10/16.
 */
public class CommandHelper {
    private static final Logger LOGGER = Logger.getLogger(CommandHelper.class);
    private static final CommandHelper instance = new CommandHelper();
    private Map<CommandName, ActionCommand> commands = new HashMap<>();

    private CommandHelper() {
        commands.put(CommandName.ADD_MEDICATION, new AddMedicationCommand());
        commands.put(CommandName.ADD_RECIPE, new AddRecipeCommand());
        commands.put(CommandName.DELETE_RECIPE, new DeleteRecipeCommand());
        commands.put(CommandName.DELETE_MEDICATION, new DeleteMedicationCommand());
        commands.put(CommandName.FILTER_MEDICATIONS, new FilterMedicationsListCommand());
        commands.put(CommandName.LOAD_DATA_TO_EDIT_MEDICATION, new LoadDataToEditMedicationCommand());
        commands.put(CommandName.LOAD_DATA_TO_EDIT_RECIPE, new LoadDataToEditRecipeCommand());
        commands.put(CommandName.OPEN_MEDICATION, new OpenMedicationCommand());
        commands.put(CommandName.OPEN_RECIPE, new OpenRecipeCommand());
        commands.put(CommandName.OPEN_SALE, new OpenSaleCommand());
        commands.put(CommandName.SIGN_IN, new SignInCommand());
        commands.put(CommandName.SIGN_OUT, new SignOutCommand());
        commands.put(CommandName.REGISTER, new RegisterCommand());
        commands.put(CommandName.OPEN_USER, new OpenUserCommand());
        commands.put(CommandName.ADD_SALE, new AddSaleCommand());
        commands.put(CommandName.UPDATE_MEDICATION, new UpdateMedicationCommand());
        commands.put(CommandName.UPDATE_RECIPE, new UpdateRecipeCommand());
        commands.put(CommandName.DELETE_USER, new DeleteUserCommand());
        commands.put(CommandName.CHANGE_LOCALE, new ChangeLocaleCommand());
        commands.put(CommandName.WRONG_COMMAND, new WrongCommand());
    }

    public static CommandHelper getInstance() { return instance; }

    public ActionCommand getCommand(String commandName) {
        CommandName name = null;
        try {
            name = CommandName.valueOf(commandName.toUpperCase());
        } catch (IllegalArgumentException e) {
            LOGGER.error("Enum constant with name" + commandName + " isn't presented");
        }
        ActionCommand command;
        if (name != null) {
            command = commands.get(name);
        } else {
            command = commands.get(CommandName.WRONG_COMMAND);
        }
        return command;
    }
}
