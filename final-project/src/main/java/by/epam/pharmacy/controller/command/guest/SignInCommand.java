package by.epam.pharmacy.controller.command.guest;

import by.epam.pharmacy.controller.command.ActionCommand;
import by.epam.pharmacy.controller.command.exception.CommandException;
import by.epam.pharmacy.controller.command.common.OpenMedicationCommand;
import by.epam.pharmacy.dao.CrudDao;
import by.epam.pharmacy.dao.DaoException;
import by.epam.pharmacy.dao.DaoFactory;
import by.epam.pharmacy.dao.DaoFactoryImpl;
import by.epam.pharmacy.dao.impl.DaoUsers;
import by.epam.pharmacy.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by a.gladkikh on 09/10/16.
 */
public class SignInCommand implements ActionCommand {
    private static final String LOGIN ="login";
    private static final String PASSWORD = "password";
    private static final String USER = "user";

    @SuppressWarnings("unchecked")
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        DaoFactory daoFactory = DaoFactoryImpl.getInstance();
        CrudDao dao = daoFactory.getDAO(DaoFactoryImpl.DaoType.USERS.name());
        if (dao.getClass() == DaoUsers.class) {
            String login = request.getParameter(LOGIN);
            String password = request.getParameter(PASSWORD);
            try {
                User user = ((DaoUsers) dao).getUserByLoginAndPassword(login, password);
                if (user != null) {
                    session.setAttribute(USER, user);
                } else {
                    session.setAttribute(USER, new User());
                }
            } catch (DaoException e) {
                throw new CommandException("Can't get account with login:" + login + ", password:" + password, e);
            }
        }
        return new OpenMedicationCommand().execute(request, response);
    }
}
