package by.epam.pharmacy.controller.command.other;

import by.epam.pharmacy.controller.PageHelper;
import by.epam.pharmacy.controller.command.ActionCommand;
import by.epam.pharmacy.controller.command.exception.CommandException;
import by.epam.pharmacy.dao.CrudDao;
import by.epam.pharmacy.dao.DaoException;
import by.epam.pharmacy.dao.DaoFactory;
import by.epam.pharmacy.dao.DaoFactoryImpl;
import by.epam.pharmacy.dao.impl.DaoSales;
import by.epam.pharmacy.entity.Sale;
import by.epam.pharmacy.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by a.gladkikh on 12/10/16.
 */
public class OpenSaleCommand implements ActionCommand {
    private static final String USER = "user";
    private static final String SALES = "sales";

    @SuppressWarnings("unchecked")
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String page = PageHelper.getInstance().getProperty(PageHelper.SALES_PAGE);
        DaoFactory factory = DaoFactoryImpl.getInstance();
        List<Sale> sales;
        HttpSession session = request.getSession(true);
        User user = (User) session.getAttribute(USER);
        if (user.getStatus().getName().compareToIgnoreCase("pharmacist") == 0) {
            try {
                sales = factory.getDAO(DaoFactoryImpl.DaoType.USERS.name()).readAll();
                request.setAttribute(SALES, sales);
            } catch (DaoException e) {
                throw new CommandException("Can't load list of sales", e);
            }
        } else if (user.getStatus().getName().compareToIgnoreCase("customer") == 0) {
            try {
                CrudDao<Sale> dao = factory.getDAO(DaoFactoryImpl.DaoType.USERS.name());
                sales = ((DaoSales) dao).getSalesByUser(user.getId());
                request.setAttribute(SALES, sales);
            } catch (DaoException e) {
                throw new CommandException("Can't load concrete user's recipes", e);
            }
        }
        return page;
    }
}
