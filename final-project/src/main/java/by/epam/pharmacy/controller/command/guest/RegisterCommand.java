package by.epam.pharmacy.controller.command.guest;

import com.fasterxml.jackson.databind.ObjectMapper;
import by.epam.pharmacy.controller.command.ActionCommand;
import by.epam.pharmacy.controller.command.exception.CommandException;
import by.epam.pharmacy.dao.DaoException;
import by.epam.pharmacy.dao.DaoFactoryImpl;
import by.epam.pharmacy.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by a.gladkikh on 09/10/16.
 */
public class RegisterCommand implements ActionCommand {
    private static final String JSON = "json";

    @SuppressWarnings("unchecked")
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String json = (String) request.getAttribute(JSON);
        ObjectMapper mapper = new ObjectMapper();
        try {
            User user = mapper.readValue(json, User.class);
            try {
                DaoFactoryImpl.getInstance().getDAO(DaoFactoryImpl.DaoType.USERS.name()).create(user);
            } catch (DaoException e) {
                mapper.writeValue(response.getOutputStream(), -1);
            }
            mapper.writeValue(response.getOutputStream(), 1);
        } catch (IOException e) {
            throw new CommandException(e);
        }
        return null;
    }
}
