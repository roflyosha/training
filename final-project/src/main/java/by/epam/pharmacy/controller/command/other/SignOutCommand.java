package by.epam.pharmacy.controller.command.other;

import by.epam.pharmacy.controller.command.ActionCommand;
import by.epam.pharmacy.controller.command.common.OpenMedicationCommand;
import by.epam.pharmacy.controller.command.exception.CommandException;
import by.epam.pharmacy.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by a.gladkikh on 01/09/16.
 */
public class SignOutCommand implements ActionCommand {
    private static final String USER = "user";

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        HttpSession session = request.getSession();
        String page = new OpenMedicationCommand().execute(request, response);
        session.setAttribute(USER, new User());
        return page;
    }
}
