package by.epam.pharmacy.controller.listener;

import by.epam.pharmacy.entity.User;

import javax.servlet.annotation.WebListener;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by a.gladkikh on 10/10/16.
 */
@WebListener
public class SessionCreatingListener implements HttpSessionListener {
    private static final String USER = "user";
    private static final String LOCALE = "locale";
    private static ReentrantLock lock = new ReentrantLock();

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        HttpSession session = se.getSession();
        lock.lock();
        try {
            session.setAttribute(USER, new User());
            session.setAttribute(LOCALE, "en_US");
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {

    }
}
