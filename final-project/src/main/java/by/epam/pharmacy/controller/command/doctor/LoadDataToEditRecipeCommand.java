package by.epam.pharmacy.controller.command.doctor;

import by.epam.pharmacy.controller.PageHelper;
import by.epam.pharmacy.controller.command.ActionCommand;
import by.epam.pharmacy.controller.command.exception.CommandException;
import by.epam.pharmacy.dao.DaoException;
import by.epam.pharmacy.dao.DaoFactory;
import by.epam.pharmacy.dao.DaoFactoryImpl;
import by.epam.pharmacy.entity.Medication;
import by.epam.pharmacy.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by a.gladkikh on 12/10/16.
 */
public class LoadDataToEditRecipeCommand implements ActionCommand {
    private static final String USERS = "users";
    private static final String MEDICATIONS = "medications";

    @SuppressWarnings("unchecked")
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String page = PageHelper.getInstance().getProperty(PageHelper.EDIT_RECIPE_PAGE);
        DaoFactory factory = DaoFactoryImpl.getInstance();
        try {
            List<User> users = factory.getDAO(DaoFactoryImpl.DaoType.USERS.name()).readAll();
            request.setAttribute(USERS, users);
        } catch (DaoException e) {
            throw new CommandException("Can't load list of users", e);
        }
        try {
            List<Medication> medications = factory.getDAO(DaoFactoryImpl.DaoType.MEDICATIONS.name()).readAll();
            request.setAttribute(MEDICATIONS, medications);
        } catch (DaoException e) {
            throw new CommandException("Can't load list of medications", e);
        }
        return page;
    }
}
