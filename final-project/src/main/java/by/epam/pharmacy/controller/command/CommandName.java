package by.epam.pharmacy.controller.command;

/**
 * Created by a.gladkikh on 07/09/16.
 */
public enum CommandName {
    SIGN_IN,
    SIGN_OUT,
    REGISTER,
    OPEN_MEDICATION,
    OPEN_RECIPE,
    OPEN_SALE,
    OPEN_USER,
    ADD_MEDICATION,
    ADD_RECIPE,
    ADD_SALE,
    UPDATE_MEDICATION,
    UPDATE_RECIPE,
    DELETE_RECIPE,
    DELETE_MEDICATION,
    DELETE_USER,
    FILTER_MEDICATIONS,
    LOAD_DATA_TO_EDIT_MEDICATION,
    LOAD_DATA_TO_EDIT_RECIPE,
    CHANGE_LOCALE,
    WRONG_COMMAND
}
