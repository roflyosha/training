package by.epam.pharmacy.controller.command.admin;

import by.epam.pharmacy.controller.PageHelper;
import by.epam.pharmacy.controller.command.ActionCommand;
import by.epam.pharmacy.controller.command.exception.CommandException;
import by.epam.pharmacy.dao.DaoException;
import by.epam.pharmacy.dao.DaoFactory;
import by.epam.pharmacy.dao.DaoFactoryImpl;
import by.epam.pharmacy.entity.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by a.gladkikh on 12/10/16.
 */
public class OpenUserCommand implements ActionCommand {
    private static final String USERS = "users";

    @SuppressWarnings("unchecked")
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
        String page = PageHelper.getInstance().getProperty(PageHelper.USERS_PAGE);
        DaoFactory factory = DaoFactoryImpl.getInstance();
        try {
            List<User> users= factory.getDAO(DaoFactoryImpl.DaoType.USERS.name()).readAll();
            request.setAttribute(USERS, users);
        } catch (DaoException e) {
            throw new CommandException("Can't load list of sales", e);
        }
        return page;
    }
}
