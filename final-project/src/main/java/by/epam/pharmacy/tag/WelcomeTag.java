package by.epam.pharmacy.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * The WelcomeTag class describes how to handle custom tag <welcome/>
 *
 * @author dmitr_000
 * @version 1.00 08-Jul-15
 */
public class WelcomeTag extends TagSupport {
    private String status;
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int doStartTag() throws JspException {
        String message = null;
        if (!"guest".equalsIgnoreCase(status)) {
            message = "Hi, " + name + "!";
        } else {
            message = "";
        }
        try {
            pageContext.getOut().write(message);
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }
}
