package by.epam.pharmacy.entity;

import by.epam.pharmacy.dao.DaoException;
import by.epam.pharmacy.dao.DaoFactoryImpl;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by a.gladkikh on 14/09/16.
 */
public class User {
    private Long id;
    private String login;
    private String password;
    private String name;
    private String surname;
    private Status status;

    public User(){
        Status status1 = new Status();
        status1.setName("guest");
        status = status1;
        login = "guest";
    }

//    public static void main(String[] args) throws DaoException {
//        User user = new User();
//        Status status = new Status();
//        user.setLogin("maks321");
//        user.setPassword("123");
//        user.setName("Maksim");
//        user.setSurname("Oshepkov");
//        status.setId(4L);
//        status.setName("doctor");
//        user.setStatus(status);
//        DaoFactoryImpl.getInstance().getDAO(DaoFactoryImpl.DaoType.USERS.name()).create(user);
//    }

    public static String hashFromPassword(String initialPassword) {
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(initialPassword.getBytes());
            byte[] bytes = md.digest();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return generatedPassword;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = hashFromPassword(password);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (!id.equals(user.id)) return false;
        if (!login.equals(user.login)) return false;
        if (!password.equals(user.password)) return false;
        if (!name.equals(user.name)) return false;
        if (!surname.equals(user.surname)) return false;
        return status.equals(user.status);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + login.hashCode();
        result = 31 * result + password.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + surname.hashCode();
        result = 31 * result + status.hashCode();
        return result;
    }
}
