package by.epam.pharmacy.entity;

import java.sql.Date;

/**
 * Created by a.gladkikh on 14/09/16.
 */
public class Sale  {
    private Long id;
    private Medication medication;
    private User user;
    private int quantity;
    private String phase;
    private Date dateOfSale;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getPhase() {
        return phase;
    }

    public void setPhase(String phase) {
        this.phase = phase;
    }

    public Date getDateOfSale() {
        return dateOfSale;
    }

    public void setDateOfSale(Date dateOfSale) {
        this.dateOfSale = dateOfSale;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Sale sale = (Sale) o;

        if (quantity != sale.quantity) return false;
        if (!id.equals(sale.id)) return false;
        if (!medication.equals(sale.medication)) return false;
        if (!user.equals(sale.user)) return false;
        if (!phase.equals(sale.phase)) return false;
        return dateOfSale.equals(sale.dateOfSale);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + medication.hashCode();
        result = 31 * result + user.hashCode();
        result = 31 * result + quantity;
        result = 31 * result + phase.hashCode();
        result = 31 * result + dateOfSale.hashCode();
        return result;
    }
}
