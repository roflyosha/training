package by.epam.pharmacy.entity;

import java.sql.Timestamp;

/**
 * Created by a.gladkikh on 14/09/16.
 */
public class Recipe {
    private Long id;
    private User user;
    private Medication medication;
    private boolean freeOfCharge;
    private Timestamp expirationDate;
    private boolean needExtension;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    public boolean isFreeOfCharge() {
        return freeOfCharge;
    }

    public void setFreeOfCharge(byte freeOfCharge) {
        if (freeOfCharge <= 0)
            this.freeOfCharge = false;
        else
            this.freeOfCharge = true;
    }

    public Timestamp getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Timestamp expirationDate) {
        this.expirationDate = expirationDate;
    }

    public boolean isNeedExtension() {
        return needExtension;
    }

    public void setNeedExtension(byte needExtension) {
        if (needExtension <= 0)
            this.needExtension = false;
        else
            this.needExtension = true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Recipe recipe = (Recipe) o;

        if (freeOfCharge != recipe.freeOfCharge) return false;
        if (needExtension != recipe.needExtension) return false;
        if (!id.equals(recipe.id)) return false;
        if (!user.equals(recipe.user)) return false;
        if (!medication.equals(recipe.medication)) return false;
        return expirationDate.equals(recipe.expirationDate);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + user.hashCode();
        result = 31 * result + medication.hashCode();
        result = 31 * result + (freeOfCharge ? 1 : 0);
        result = 31 * result + expirationDate.hashCode();
        result = 31 * result + (needExtension ? 1 : 0);
        return result;
    }
}
