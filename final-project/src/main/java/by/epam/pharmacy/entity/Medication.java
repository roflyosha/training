package by.epam.pharmacy.entity;

/**
 * Created by a.gladkikh on 14/09/16.
 */
public class Medication {
    private Long id;
    private String title;
    private String instructions;
    private boolean needRecipe;
    private float quantity;
    private float unitPrice;
    private Category category;
    private Manufacturer manufacturer;
    private Application application;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public boolean isNeedRecipe() {
        return needRecipe;
    }

    public void setNeedRecipe(byte needRecipe) {
        if (needRecipe <= 0)
            this.needRecipe = false;
        else
            this.needRecipe = true;
    }

    public float getQuantity() {
        return quantity;
    }

    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }

    public float getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(float unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(Manufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Medication that = (Medication) o;

        if (needRecipe != that.needRecipe) return false;
        if (Float.compare(that.quantity, quantity) != 0) return false;
        if (Float.compare(that.unitPrice, unitPrice) != 0) return false;
        if (!id.equals(that.id)) return false;
        if (!title.equals(that.title)) return false;
        if (!instructions.equals(that.instructions)) return false;
        if (!category.equals(that.category)) return false;
        if (!manufacturer.equals(that.manufacturer)) return false;
        return application.equals(that.application);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + title.hashCode();
        result = 31 * result + instructions.hashCode();
        result = 31 * result + (needRecipe ? 1 : 0);
        result = 31 * result + (quantity != +0.0f ? Float.floatToIntBits(quantity) : 0);
        result = 31 * result + (unitPrice != +0.0f ? Float.floatToIntBits(unitPrice) : 0);
        result = 31 * result + category.hashCode();
        result = 31 * result + manufacturer.hashCode();
        result = 31 * result + application.hashCode();
        return result;
    }
}
