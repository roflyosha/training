<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<a href="index.jsp">Back to index.jsp</a>
<table border="1">
    <tr>
        <td>id</td>
        <td>login</td>
        <td>password</td>
        <td>name</td>
        <td>surname</td>
        <td>status</td>
    </tr>
    <%--<c:forEach items="${list}" var="item">--%>
        <tr>
            <td>${user.id}</td>
            <td>${user.login}</td>
            <td>${user.password}</td>
            <td>${user.name}</td>
            <td>${user.surname}</td>
            <td>${user.status.name}</td>
        </tr>
    <%--</c:forEach>--%>
</table>
</body>
</html>
