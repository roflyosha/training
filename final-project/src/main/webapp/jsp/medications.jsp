<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Medications</title>
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/font-awesome.min.css" rel="stylesheet">
</head>

<body>
<header class="navbar navbar-fixed-top">
    <c:set var="locale" value="${locale}"/>
    <fmt:setLocale value="${locale}"/>
    <fmt:setBundle basename="messages"/>
    <jsp:useBean id="user" type="by.epam.pharmacy.entity.User" scope="session"/>
    <c:if test="${!empty user}">
        <c:set var="authResult" value="${user.status.name}"/>
    </c:if>
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle xs-menu" data-toggle="collapse" data-target="#responsive-menu">
                <span class="sr-only">Открыть навигацию</span>
                <i class="fa fa-bars fa-lg" aria-hidden="true"></i>
            </button>
            <a href="#" class="navbar-brand"><img class="logo"
                                                  src="${pageContext.request.contextPath}/images/logo_pharmacy.png"
                                                  alt=""></a>
        </div>
        <div class="collapse navbar-collapse menu" id="responsive-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><fmt:message
                        key="nav.medications"/></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="javascript:;"
                               onclick="document.getElementById('cat-sel').value = '0';
                                    document.getElementById('filter-medications').submit();">
                                <c:out value="Все"/>
                            </a>
                        </li>
                        <c:forEach items="${categories}" var="category">
                            <li>
                                <a href="javascript:;"
                                   onclick="document.getElementById('cat-sel').value = '${category.id}';
                                           document.getElementById('filter-medications').submit();">
                                    <c:out value="${category.name}"/>
                                </a>
                            </li>
                        </c:forEach>
                    </ul>
                </li>
                <c:if test="${authResult == 'customer' || authResult == 'pharmacist'}">
                    <li><a href="javascript:;" onclick="document.getElementById('open-sale').submit();">
                        <fmt:message key="nav.sales"/></a></li>
                </c:if>
                <c:if test="${authResult == 'doctor' || authResult == 'customer'}">
                    <li><a href="javascript:;" onclick="document.getElementById('open-recipe').submit();">
                        <fmt:message key="nav.recipes"/></a></li>
                </c:if>
                <c:if test="${authResult == 'admin'}">
                    <li><a href="javascript:;" onclick="document.getElementById('open-user').submit();">
                        <fmt:message key="nav.users"/></a></li>
                </c:if>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <c:if test="${authResult != 'guest'}">
                            <c:out value="${'Hello, '.concat(user.name).concat(' ').concat(user.surname)}"/>
                        </c:if>
                        <c:if test="${authResult == 'guest'}">
                            <c:out value="Log in"/>
                        </c:if>
                    </a>
                    <c:if test="${authResult != 'guest'}">
                        <ul id="login-dp" class="dropdown-menu">
                            <li>
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <a href="#">My account</a>
                                    </div>
                                    <div class="bottom text-center">
                                        <a href="#" data-toggle="modal" data-target="#modal-2"><b>Exit site</b></a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </c:if>
                    <c:if test="${authResult == 'guest'}">
                        <ul id="login-dp" class="dropdown-menu">
                            <li>
                                <div class="row">
                                    <div class="col-md-12">
                                        <form action="controller" method="post" id="sign-in-form">
                                            <div class="form-group">
                                                <label class="sr-only" for="login">login</label>
                                                <input type="text" form="sign-in-form" name="login" class="form-control"
                                                       id="login"
                                                       placeholder="Your login" required>
                                            </div>
                                            <div class="form-group">
                                                <label class="sr-only" for="password">password</label>
                                                <input type="password" form="sign-in-form" name="password"
                                                       class="form-control" id="password"
                                                       placeholder="Your password" required>
                                            </div>
                                            <div class="form-group">
                                                <button type="submit" form="sign-in-form"
                                                        class="btn btn-primary btn-block">
                                                    Log in
                                                </button>
                                            </div>
                                            <input type="hidden" name="command" value="sign_in">
                                        </form>
                                    </div>
                                    <div class="bottom text-center">
                                        Haven't account? <a href="#" data-toggle="modal" data-target="#modal-1"><b>Join
                                        Us</b></a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </c:if>
                </li>
            </ul>
        </div>
    </div>
</header>
<br>
<br>
<br>
<div class="container after-header">
    <br>
    <div class="row">
        <c:if test="${empty medications}">
            <c:out value="Ни одна запись не соответствует запросу"/>
        </c:if>
        <c:forEach items="${medications}" var="medication">
            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>${medication.title}</h3>
                        <p>${medication.instructions}</p>
                        <h4><b>${medication.unitPrice}</b></h4>
                        <span><a href="#" class="btn btn-success">Подробнее
                                <i class="fa fa-arrow-right"></i></a></span>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>
</div>
<div class="modal fade" id="modal-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Please, register in our system</h4>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form action="controller" method="post" id="register-form">
                            <div class="form-group">
                                <label class="sr-only" for="name">Your name</label>
                                <input type="text" form="register-form" name="name" class="form-control" id="name"
                                       placeholder="Type your name"
                                       required>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="surname">Your surname</label>
                                <input type="text" form="register-form" name="surname" class="form-control" id="surname"
                                       placeholder="And your surname" required>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="login-reg">Login</label>
                                <input type="text" form="register-form" name="login" class="form-control" id="login-reg"
                                       placeholder="Your unique login" required>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="password-reg">Password</label>
                                <input type="password" form="register-form" name="password" class="form-control"
                                       id="password-reg"
                                       placeholder="Password" required>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-block">Register account</button>
                            </div>
                            <input type="hidden" name="command" value="register">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-2">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Are you sure want to leave?</h3>
            </div>
            <div class="modal-footer">
                <input type="button" class="btn btn-success" form="sign-out"
                       onclick="document.getElementById('sign-out').submit();"
                       value="Yes" data-dismiss="modal"/>
                <input type="button" class="btn btn-danger" value="No" data-dismiss="modal"/>
            </div>
        </div>
    </div>
</div>
<div>
    <form action="controller" id="open-user" method="get">
        <input type="hidden" name="command" value="open_user">
    </form>
    <form action="controller" id="open-medication" method="get">
        <input type="hidden" name="command" value="open_medication">
    </form>
    <form action="controller" id="open-sale" method="get">
        <input type="hidden" name="command" value="open_sale">
    </form>
    <form action="controller" id="open-recipe" method="get">
        <input type="hidden" name="command" value="open_recipe">
    </form>
    <form action="controller" id="sign-out" method="post">
        <input type="hidden" name="command" value="sign_out">
    </form>
    <form action="controller" id="filter-medications" method="get">
        <input type="hidden" id="cat-sel" name="categorySelected" form="filter-medications">
        <input type="hidden" name="command" value="filter_medications">
    </form>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
</body>

</html>
