<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sales</title>
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/font-awesome.min.css" rel="stylesheet">
</head>
<body><header class="navbar navbar-fixed-top">
    <c:set var="locale" value="${locale}"/>
    <fmt:setLocale value="${locale}"/>
    <fmt:setBundle basename="messages"/>
    <jsp:useBean id="user" type="by.epam.pharmacy.entity.User" scope="session"/>
    <c:if test="${!empty user}">
        <c:set var="authResult" value="${user.status.name}"/>
    </c:if>
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle xs-menu" data-toggle="collapse" data-target="#responsive-menu">
                <span class="sr-only">Открыть навигацию</span>
                <i class="fa fa-bars fa-lg" aria-hidden="true"></i>
            </button>
            <a href="#" class="navbar-brand"><img class="logo"
                                                  src="${pageContext.request.contextPath}/images/logo_pharmacy.png"
                                                  alt=""></a>
        </div>
        <div class="collapse navbar-collapse menu" id="responsive-menu">
            <ul class="nav navbar-nav">
                <li><a href="javascript:;" onclick="document.getElementById('open-medication').submit();">
                    <fmt:message key="nav.medications"/></a></li>
                <li><a href="javascript:;" onclick="document.getElementById('open-sale').submit();">
                    <fmt:message key="nav.sales"/></a></li>
                <c:if test="${authResult == 'customer'}">
                    <li><a href="javascript:;" onclick="document.getElementById('open-recipe').submit();">
                        <fmt:message key="nav.recipes"/></a></li>
                </c:if>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <c:out value="${'Hello, '.concat(user.name).concat(' ').concat(user.surname)}"/>
                    </a>
                    <ul id="login-dp" class="dropdown-menu">
                        <li>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <a href="#">My account</a>
                                </div>
                                <div class="bottom text-center">
                                    <a href="#" data-toggle="modal" data-target="#modal-1"><b>Exit site</b></a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</header>
<br>
<br>
<br>
<div class="container after-header">
    <br>
    <div class="row">
        <c:forEach items="${users}" var="userEntity">
            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>${userEntity.name}${userEntity.surname}</h3>
                        <p><c:out value="User login: "/>${medication.instructions}</p>
                        <h4><b><c:out value="Status: "/>${userEntity.status.name}</b></h4>
                        <span><a href="#" class="btn btn-success">Удалить</a></span>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>
</div>
<div class="modal fade" id="modal-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Are you sure want to leave?</h3>
            </div>
            <div class="modal-footer">
                <input type="button" class="btn btn-success" form="sign-out"
                       onclick="document.getElementById('sign-out').submit();"
                       value="Yes" data-dismiss="modal"/>
                <input type="button" class="btn btn-danger" value="No" data-dismiss="modal"/>
            </div>
        </div>
    </div>
</div>
<div>
    <form action="controller" id="open-medication" method="get">
        <input type="hidden" name="command" value="open_medication">
    </form>
    <form action="controller" id="open-user" method="get">
        <input type="hidden" name="command" value="open_user">
    </form>
    <form action="controller" id="sign-out" method="post">
        <input type="hidden" name="command" value="sign_out">
    </form>
</div>
<script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script></body>
</html>
