<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="ru">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap Template</title>
    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/style.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/font-awesome.min.css" rel="stylesheet">
</head>

<body>
    <header class="navbar navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle xs-menu" data-toggle="collapse" data-target="#responsive-menu">
                    <span class="sr-only">Открыть навигацию</span>
                    <i class="fa fa-bars fa-lg" aria-hidden="true"></i>
                </button>
                <a href="#" class="navbar-brand"><img class="logo" src="/images/logo_pharmacy.png"></a>
            </div>
            <div class="collapse navbar-collapse menu" id="responsive-menu">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="userLogo"></i></a>
                        <ul id="login-dp" class="dropdown-menu">
                            <li>
                                <div class="row">
                                    <div class="col-md-12">
                                        <form class="form" role="form" method="post" action="login" accept-charset="UTF-8" id="login-nav">
                                            <div class="form-group">
                                                <label class="sr-only" for="exampleInputEmail2">Email address</label>
                                                <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Email address" required>
                                            </div>
                                            <div class="form-group">
                                                <label class="sr-only" for="exampleInputPassword2">Password</label>
                                                <input type="password" class="form-control" id="exampleInputPassword2" placeholder="Password" required>
                                            </div>
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary btn-block">Log in</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="bottom text-center">
                                        Haven't account? <a href="#" data-toggle="modal" data-target="#modal-1"><b>Join Us</b></a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </header>
    <div class="container-fluid fill">
        <div id="carousel" class="carousel slide">
            <!--       Индикаторы слайдов-->
            <ol class="carousel-indicators">
                <li class="active" data-target="#carousel" data-slide-to="0"></li>
                <li data-target="#carousel" data-slide-to="1"></li>
                <li data-target="#carousel" data-slide-to="2"></li>
            </ol>
            <!--        Слайды-->
            <div class="carousel-inner">
                <div class="item active fill">
                    <div class="fill"><img src="/images/pharmacy-1.jpg" alt=""></div>
                    <div class="carousel-caption">
                        <h3>Первый слайд</h3>
                        <p>Описание первого слайда</p>
                    </div>
                </div>
                <div class="item fill">
                    <div class="fill"><img src="/images/pharmacy-2.jpg" alt=""></div>
                    <div class="carousel-caption">
                        <h3>Второй слайд</h3>
                        <p>Описание второго слайда</p>
                    </div>
                </div>
                <div class="item fill">
                    <div class="fill"><img src="/images/pharmacy-3.jpg" alt=""></div>
                    <div class="carousel-caption">
                        <h3>Третий слайд</h3>
                        <p>Описание третьего слайда</p>
                    </div>
                </div>
            </div>
            <!--        Стрелки переключения слайдов-->
            <a href="#carousel" class="left carousel-control" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a href="#carousel" class="right carousel-control" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </div>
    </div>
    <div class="modal fade" id="modal-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">       
                    <h4 class="modal-title">Please, register in our system</h4>
                </div>
                
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <form class="form" role="form" method="post" action="login" accept-charset="UTF-8" id="login-nav">
                               <div class="form-group">
                                    <label class="sr-only" for="exampleInputName">Your name</label>
                                    <input type="text" class="form-control" id="exampleInputName" placeholder="Your name" required>
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="exampleInputSurname">Your surname</label>
                                    <input type="text" class="form-control" id="exampleInputSurname" placeholder="Your surname" required>
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="exampleInputEmail">Email address</label>
                                    <input type="email" class="form-control" id="exampleInputEmail" placeholder="Email address" required>
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="exampleInputPassword">Password</label>
                                    <input type="password" class="form-control" id="exampleInputPassword" placeholder="Password" required>
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="exampleInputPasswordrepeat">Repeat password</label>
                                    <input type="password" class="form-control" id="exampleInputPasswordRepeat" placeholder="Repeat password" required>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">Register account</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
</body>

</html>