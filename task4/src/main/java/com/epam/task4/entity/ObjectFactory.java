
package com.epam.task4.entity;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.epam.task4.entity package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.epam.task4.entity
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Flowers }
     * 
     */
    public Flowers createFlowers() {
        return new Flowers();
    }

    /**
     * Create an instance of {@link Flowers.Flower }
     * 
     */
    public Flowers.Flower createFlowersFlower() {
        return new Flowers.Flower();
    }

    /**
     * Create an instance of {@link Flowers.Flower.VisualParameters }
     * 
     */
    public Flowers.Flower.VisualParameters createFlowersFlowerVisualParameters() {
        return new Flowers.Flower.VisualParameters();
    }

    /**
     * Create an instance of {@link Flowers.Flower.GrowingTips }
     * 
     */
    public Flowers.Flower.GrowingTips createFlowersFlowerGrowingTips() {
        return new Flowers.Flower.GrowingTips();
    }

}
