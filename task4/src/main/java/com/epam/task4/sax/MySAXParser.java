package com.epam.task4.sax;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;

/**
 * Created by a.gladkikh on 16/07/16.
 */
public class MySAXParser {
    public static void main(String[] args) {
        try {
            SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
            SAXParser saxParser = saxParserFactory.newSAXParser();

            DefaultHandler handler = new DefaultHandler() {
                String id = "";
                String tagName = "";

                @Override
                public void startDocument() throws SAXException {
                    System.out.println("Начало обработки xml-документа...");
                }

                @Override
                public void endDocument() throws SAXException {
                    System.out.println("Конец обработки xml-документа...");
                }

                @Override
                public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
//                    System.out.println("Чтение элемента " + qName);
                    tagName = qName;
                    if(qName.equals("flower")) {
                        id = attributes.getValue(0);
                        System.out.println("Flower id: " + id);
                    }
                }

                @Override
                public void endElement(String uri, String localName, String qName) throws SAXException {
                    tagName = "";
                    id = "";
                }

                @Override
                public void characters(char[] ch, int start, int length) throws SAXException {
                    if (tagName.equals("name")) {
                        System.out.println("Name: " + new String(ch, start, length));
                    }
                    if (tagName.equals("soil")) {
                        System.out.println("Soil: " + new String(ch, start, length));
                    }
                    if (tagName.equals("origin")) {
                        System.out.println("Origin: " + new String(ch, start, length));
                    }
                    if (tagName.equals("leavesColor")) {
                        System.out.println("Leaves color: " + new String(ch, start, length));
                    }
                    if (tagName.equals("flowerSize")) {
                        System.out.println("Flower size: " + new String(ch, start, length));
                    }
                    if (tagName.equals("temperature")) {
                        System.out.println("Temperature: " + new String(ch, start, length));
                    }
                    if (tagName.equals("lighting")) {
                        System.out.println("Lighting: " + new String(ch, start, length));
                    }
                    if (tagName.equals("watering")) {
                        System.out.println("Watering: " + new String(ch, start, length));
                    }
                    if (tagName.equals("multiplying")) {
                        System.out.println("Multiplying: " + new String(ch, start, length));
                        System.out.println();
                    }

                }
            };
            saxParser.parse("src/main/resources/flowers.xml", handler);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }

    public static void characters() {
//        if (name) {
//            System.out.println("Name: " + new String(ch, start, length));
//            name = false;
//        }
//        if (soil) {
//            System.out.println("Soil: " + new String(ch, start, length));
//            soil = false;
//        }
//        if (origin) {
//            System.out.println("Origin: " + new String(ch, start, length));
//            origin = false;
//        }
//        if (leavesColor) {
//            System.out.println("Leaves color: " + new String(ch, start, length));
//            leavesColor = false;
//        }
//        if (flowerSize) {
//            System.out.println("Flower size: " + new String(ch, start, length));
//            flowerSize = false;
//
//        }
//
//
//        if (temperature) {
//            System.out.println("Temperature: " + new String(ch, start, length));
//            temperature = false;
//        }
//        if (lighting) {
//            System.out.println("Lighting: " + new String(ch, start, length));
//            lighting = false;
//        }
//        if (watering) {
//            System.out.println("Watering: " + new String(ch, start, length));
//            watering = false;
//
//        }
//
//        if (multiplying) {
//            System.out.println("Multiplying: " + new String(ch, start, length));
//            multiplying = false;
//        }
    }
}
