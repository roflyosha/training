package com.epam.task4.xmlValidation;

import org.xml.sax.SAXException;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

/**
 * Created by a.gladkikh on 16/07/16.
 */
public class Validation {
    public static void main(String[] args) throws SAXException, IOException {
        try {
            SchemaFactory factory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
            File schemaLocation = new File("src/main/resources/schema.xsd");
            Schema schema = factory.newSchema(schemaLocation);
            Validator validator = schema.newValidator();
            Source source = new StreamSource("src/main/resources/flowers.xml");
            validator.validate(source);
            System.out.println("XML file is valid.");
        } catch (SAXException ex) {
            System.out.println("XML file is not valid because ");
            System.out.println(ex.getMessage());
        }

    }
}
