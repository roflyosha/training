package com.epam.task4.dom;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

/**
 * Created by a.gladkikh on 12/07/16.
 */
public class DOMParser {
    public static void main(String[] args) {
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            org.w3c.dom.Document document = documentBuilder.parse("src/main/resources/flowers.xml");
            String rootElement = document.getDocumentElement().getNodeName();
            System.out.println("Root element: " + rootElement);
            NodeList nodeList = document.getDocumentElement().getChildNodes();
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    System.out.println();
                    Element element = (Element) node;
                    System.out.println(element.getTagName().toUpperCase() + ", id = " + element.getAttribute("id"));
                    NodeList nodeList1 = element.getChildNodes();
                    for (int j = 0; j < nodeList1.getLength(); j++) {
                        Node node1 = nodeList1.item(j);
                        if (node1.getNodeType() == Node.ELEMENT_NODE) {
                            String content = node1.getLastChild().getTextContent().trim();
                            switch (node1.getNodeName()) {
                                case "name":

                                    System.out.println("Name: " + content);
                                    break;
                                case "soil":
                                    System.out.println("Soil: " + content);
                                    break;
                                case "origin":
                                    System.out.println("Origin: " + content);
                                    break;
                                case "visualParameters":
                                    NodeList nodeList2 = ((Element) node1).getChildNodes();
                                    for (int x = 0; x < nodeList2.getLength(); x++) {
                                        Node node2 = nodeList2.item(x);
                                        if (node2.getNodeType() == Node.ELEMENT_NODE) {
                                            String content1 = node2.getLastChild().getTextContent().trim();
                                            switch (node2.getNodeName()) {
                                                case "leavesColor":
                                                    System.out.println("Leaves color: " + content1);
                                                    break;
                                                case "flowerSize":
                                                    System.out.println("Flower size: " + content1);
                                                    break;
                                            }
                                        }
                                    }
                                    break;
                                case "growingTips":
                                    NodeList nodeList3 = ((Element) node1).getChildNodes();
                                    for (int y = 0; y < nodeList3.getLength(); y++) {
                                        Node node3 = nodeList3.item(y);
                                        if (node3.getNodeType() == Node.ELEMENT_NODE) {
                                            String content2 = node3.getLastChild().getTextContent().trim();
                                            switch (node3.getNodeName()) {
                                                case "temperature":
                                                    System.out.println("Temperature: " + content2);
                                                    break;
                                                case "lighting":
                                                    System.out.println("Lighting: " + content2);
                                                    break;
                                                case "watering":
                                                    System.out.println("Watering: " + content2);
                                            }
                                        }
                                    }
                                    break;
                                case "multiplying":
                                    System.out.println("Multiplying: " + content);
                                    break;
                            }

                        }
                    }
                }
            }

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
