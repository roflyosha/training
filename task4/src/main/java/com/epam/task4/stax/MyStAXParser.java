package com.epam.task4.stax;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Created by a.gladkikh on 17/07/16.
 */
public class MyStAXParser {
    public static void main(String[] args) throws FileNotFoundException, XMLStreamException {
        String file = "src/main/resources/flowers.xml";
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(file));
        while (xmlEventReader.hasNext()) {
            XMLEvent xmlEvent = xmlEventReader.nextEvent();
            if (xmlEvent.isStartElement()) {
                StartElement startElement = xmlEvent.asStartElement();
                if (startElement.getName().getLocalPart().equals("flower")) {
                    System.out.println("Начало элемента Flower...");
                    System.out.println("Flower id: " + startElement.getAttributeByName(new QName("id")).getValue());
                } else if (startElement.getName().getLocalPart().equals("name")) {
                    xmlEvent = xmlEventReader.nextEvent();
                    System.out.println("Name: " + xmlEvent.asCharacters().getData());
                } else if (startElement.getName().getLocalPart().equals("soil")) {
                    xmlEvent = xmlEventReader.nextEvent();
                    System.out.println("Soil: " + xmlEvent.asCharacters().getData());
                } else if (startElement.getName().getLocalPart().equals("origin")) {
                    xmlEvent = xmlEventReader.nextEvent();
                    System.out.println("Origin: " + xmlEvent.asCharacters().getData());
                } else if (startElement.getName().getLocalPart().equals("leavesColor")) {
                    xmlEvent = xmlEventReader.nextEvent();
                    System.out.println("Leaves color: " + xmlEvent.asCharacters().getData());
                } else if (startElement.getName().getLocalPart().equals("flowerSize")) {
                    xmlEvent = xmlEventReader.nextEvent();
                    System.out.println("Flower size: " + xmlEvent.asCharacters().getData());
                } else if (startElement.getName().getLocalPart().equals("temperature")) {
                    xmlEvent = xmlEventReader.nextEvent();
                    System.out.println("Temperature: " + xmlEvent.asCharacters().getData());
                } else if (startElement.getName().getLocalPart().equals("lighting")) {
                    xmlEvent = xmlEventReader.nextEvent();
                    System.out.println("Lighting: " + xmlEvent.asCharacters().getData());
                } else if (startElement.getName().getLocalPart().equals("watering")) {
                    xmlEvent = xmlEventReader.nextEvent();
                    System.out.println("Watering: " + xmlEvent.asCharacters().getData());
                } else if (startElement.getName().getLocalPart().equals("multiplying")) {
                    xmlEvent = xmlEventReader.nextEvent();
                    System.out.println("Multiplying: " + xmlEvent.asCharacters().getData());
                }
            }
            if (xmlEvent.isEndElement()) {
                EndElement endElement = xmlEvent.asEndElement();
                if (endElement.getName().getLocalPart().equals("flower"))
                    System.out.println("Конец элемента Flower.\n");
            }
        }
    }
}
