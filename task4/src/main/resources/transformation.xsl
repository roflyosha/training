<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" encoding="utf-8" indent="no"/>

    <xsl:template match="/">
        <flowers>
            <xsl:apply-templates />
        </flowers>
    </xsl:template>

    <xsl:template match="flower">
        <xsl:element name="flower">
            <xsl:attribute name="id">
                <xsl:value-of select="@id" />
            </xsl:attribute>
            <name><xsl:value-of select="name" /></name>
            <multiplying><xsl:value-of select="multiplying" /></multiplying>
            <origin><xsl:value-of select="origin" /></origin>
            <soil><xsl:value-of select="soil" /></soil>
            <growingTips>
                <watering><xsl:value-of select="growingTips/watering"/></watering>
                <temperature><xsl:value-of select="growingTips/temperature"/></temperature>
            </growingTips>
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>