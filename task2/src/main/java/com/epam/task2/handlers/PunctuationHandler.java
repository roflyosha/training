package com.epam.task2.handlers;

import com.epam.task2.entities.PartOfText;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by a.gladkikh on 06/07/16.
 */
public class PunctuationHandler extends Handler {
    private final static Logger log = Logger.getLogger(PunctuationHandler.class.getName());
    private final static PartOfText.Type fragmentType = PartOfText.Type.PUNCTUATION;
    private static String regexp = "\\p{Punct}|\0\\n";
    private ArrayList<PartOfText> fragments = new ArrayList<>();

    public PunctuationHandler() {
        super(fragmentType);
    }

    @Override
    public String buildImpl(String str) {
        log.info("punctuation fragmentation");
        Pattern pattern = Pattern.compile(regexp);
        Matcher matcher = pattern.matcher(str);
        for (int x = 0; matcher.find(); x++) {
            this.fragments.add(new PartOfText(matcher.group(), fragmentType, matcher.start(), matcher.end()));
//            System.out.println(matcher.group() + " " + matcher.start() + " " + matcher.end());
        }
        return str;
    }

    public static PartOfText.Type getFragmentType() {
        return fragmentType;
    }

    public ArrayList<PartOfText> getFragments() {
        return fragments;
    }
}
