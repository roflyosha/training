package com.epam.task2.handlers;

import com.epam.task2.entities.PartOfText;
import org.apache.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by a.gladkikh on 06/07/16.
 */
public abstract class Handler {
    private static final Logger log = Logger.getLogger(Handler.class.getName());
    protected Handler nextFragment;
    private final PartOfText.Type fragmentType;


    public Handler(PartOfText.Type fragmentType) {
        this.fragmentType = fragmentType;
    }

    public Handler setNext(Handler fragment) {
        nextFragment = fragment;
        return fragment;
    }

    public abstract String buildImpl(String str);

    public void build(HandlingWork handlingWork, String str) {
        if (handlingWork.getFragments().contains(fragmentType))
            str = buildImpl(str);
        if (nextFragment != null)
            nextFragment.build(handlingWork, str);
    }


}
