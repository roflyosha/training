package com.epam.task2.handlers;

import com.epam.task2.entities.PartOfText;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by a.gladkikh on 06/07/16.
 */
public class WordHandler extends Handler {
    private final static Logger log = Logger.getLogger(WordHandler.class.getName());
    private final static PartOfText.Type fragmentType = PartOfText.Type.WORD;
    private static String regexp = "\\d+|([A-Za-z]+)";
    private ArrayList<PartOfText> fragments = new ArrayList<>();

    public WordHandler() {
        super(fragmentType);
    }

    public static PartOfText.Type getFragmentType() {
        return fragmentType;
    }

    @Override
    public String buildImpl(String str) {
        log.info("word fragmentation");
        Pattern pattern = Pattern.compile(regexp);
        Matcher matcher = pattern.matcher(str);
        for (int x = 0; matcher.find(); x++) {
            this.fragments.add(new PartOfText(matcher.group(), fragmentType, matcher.start(), matcher.end()));
//            System.out.println(matcher.group() + " " + matcher.start() + " " + matcher.end());
        }
        return str;
    }

    public ArrayList<PartOfText> getFragments() {
        return fragments;
    }

    public static String getRegexp() {
        return regexp;
    }
}
