package com.epam.task2.handlers;

import com.epam.task2.entities.PartOfText;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by a.gladkikh on 06/07/16.
 */
public class HandlingWork {
    private final Set<PartOfText.Type> fragments = new HashSet<>();

    public HandlingWork() {
        this.fragments.add(PartOfText.Type.CODE);
        this.fragments.add(PartOfText.Type.SENTENCE);
        this.fragments.add(PartOfText.Type.WORD);
        this.fragments.add(PartOfText.Type.PUNCTUATION);

    }

    public Set<PartOfText.Type> getFragments() {
        return fragments;
    }
}
