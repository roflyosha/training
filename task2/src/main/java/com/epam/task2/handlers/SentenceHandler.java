package com.epam.task2.handlers;

import com.epam.task2.entities.PartOfText;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by a.gladkikh on 06/07/16.
 */
public class SentenceHandler extends Handler{
    private final static Logger log = Logger.getLogger(SentenceHandler.class.getName());
    private final static PartOfText.Type fragmentType = PartOfText.Type.SENTENCE;
    private static String regexp = "(\\d+\\.)|([A-Z][^.\\n]+[.?:]*)";
    private ArrayList<PartOfText> fragments = new ArrayList<>();
    private ArrayList<PartOfText> sentenceParts = new ArrayList<>();

    public SentenceHandler() {
        super(fragmentType);
    }

    @Override
    public String buildImpl(String str) {
        log.info("sentence fragmentation");
        Pattern pattern = Pattern.compile(regexp);
        Matcher matcher = pattern.matcher(str);
        for (;matcher.find();) {
            String s = matcher.group();
            Pattern pattern1 = Pattern.compile(WordHandler.getRegexp());
            Matcher matcher1 = pattern1.matcher(s);
            PartOfText sentence = new PartOfText(matcher.group(), fragmentType, matcher.start(), matcher.end());
            sentence.initializeList();
            for (;matcher1.find();) {
                sentence.addComponent(new PartOfText(matcher1.group(), PartOfText.Type.WORD, matcher1.start(), matcher1.end()));
            }
            this.fragments.add(sentence);
//            System.out.println(matcher.group());
        }
        return str;
    }



    public static PartOfText.Type getFragmentType() {
        return fragmentType;
    }

    public ArrayList<PartOfText> getFragments() {
        return fragments;
    }
}
