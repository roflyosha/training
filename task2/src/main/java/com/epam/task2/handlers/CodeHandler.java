package com.epam.task2.handlers;

import com.epam.task2.entities.PartOfText;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by a.gladkikh on 06/07/16.
 */
public class CodeHandler extends Handler {
    private final static Logger log = Logger.getLogger(CodeHandler.class.getName());
    private final static PartOfText.Type fragmentType = PartOfText.Type.CODE;
    private static String regexp = ".+\\{[\\s\\S]{50,450}\\}";
    private ArrayList<PartOfText> fragments = new ArrayList<>();

    public CodeHandler() {
        super(fragmentType);
    }

    public static PartOfText.Type getFragmentType() {
        return fragmentType;
    }

    @Override
    public String buildImpl(String str) {
        log.info("code fragmentation");
        Pattern pattern = Pattern.compile(regexp);
        Matcher matcher = pattern.matcher(str);
        for (; matcher.find(); ) {
            this.fragments.add(new PartOfText(matcher.group(), fragmentType, matcher.start(), matcher.end()));
//            System.out.println(matcher.group());
        }
        str = str.replaceAll(regexp, "\0");
        return str;
    }

    public ArrayList<PartOfText> getFragments() {
        return fragments;
    }
}
