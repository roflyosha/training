package com.epam.task2.entities;

import java.util.ArrayList;

/**
 * Created by a.gladkikh on 06/07/16.
 */
public class PartOfText {
    private String value;
    private Type type;
    private int startIndex, endIndex;
    ArrayList<PartOfText> components;

    public PartOfText(String value, Type type, int startIndex, int endIndex) {
        this.value = value;
        this.type = type;
        this.startIndex = startIndex;
        this.endIndex = endIndex;
    }

    public String getValue() {
        return value;
    }

    public ArrayList<PartOfText> getComponents() {
        return components;
    }

    public void initializeList() {
        this.components = new ArrayList<>();
    }

    public void addComponent(PartOfText component) {
        this.components.add(component);
    }

    public Type getType() {
        return type;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public int getEndIndex() {
        return endIndex;
    }

    public enum Type {
        CODE,
        SENTENCE,
        WORD,
        PUNCTUATION
    }

}
