package com.epam.task2.runner;

import com.epam.task2.entities.PartOfText;
import com.epam.task2.handlers.*;
import com.epam.task2.util.LetterMeetings;
import com.epam.task2.util.MaxSentenceQuantity;
import com.epam.task2.util.TextRestoring;
import org.apache.log4j.Logger;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by a.gladkikh on 06/07/16.
 */
public class Runner {
    public final static Logger log = Logger.getLogger(Runner.class.getName());

    public static void main(String[] args) {
        String text = readFile("file.txt");
        Handler handler1, handler2, handler3, handler4;
        handler1 = new CodeHandler();
        handler2 = new SentenceHandler();
        handler3 = new WordHandler();
        handler4 = new PunctuationHandler();
        handler1.setNext(handler2).setNext(handler3).setNext(handler4);

        HandlingWork handlingWork = new HandlingWork();
        handler1.build(handlingWork, text);

        printRestoredText(handler3, handler4, handler1);
        printSortedWords(handler3);
        printMaxSentenceQuantity(handler2);
    }

    public static String readFile(String fileName) {
        String text = "";
        try (FileReader f = new FileReader(fileName)) {
            int c;
            StringBuilder str = new StringBuilder();
            while ((c = f.read()) != -1) {
                str.append((char) c);
            }
            text = str.toString();

        } catch (IOException e) {
            log.error(e.getMessage());
        }
        return text;
    }

    public static void printSortedWords(Handler wordHandler) {
        ArrayList<PartOfText> words = ((WordHandler) wordHandler).getFragments();
        Collections.sort(words, new LetterMeetings('a'));
        log.info("Отсортированные слова:");
        for (PartOfText elem : words)
            log.info(elem.getValue());
    }

    public static void printMaxSentenceQuantity(Handler sentenceHandler) {
        int quantity = MaxSentenceQuantity.doCounting(((SentenceHandler) sentenceHandler).getFragments());
        log.info(quantity + " предложений с одинаковыми словами");
    }

    public static void printRestoredText(Handler wordHandler, Handler punctuationHandler, Handler codeHandler) {
        String text = TextRestoring.restoreText(wordHandler, punctuationHandler, codeHandler);
        log.info("Восстановленный текст:\n" + text);
    }
}
