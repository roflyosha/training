package com.epam.task2.util;

import com.epam.task2.entities.PartOfText;
import com.epam.task2.handlers.CodeHandler;
import com.epam.task2.handlers.Handler;
import com.epam.task2.handlers.PunctuationHandler;
import com.epam.task2.handlers.WordHandler;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by a.gladkikh on 09/07/16.
 */
public class TextRestoring {

    public static String restoreText(Handler wordHandler, Handler punctuationHandler, Handler codeHandler) {
        String txt = "";
        ArrayList<PartOfText> words = ((WordHandler) wordHandler).getFragments();
        ArrayList<PartOfText> punctuations = ((PunctuationHandler) punctuationHandler).getFragments();
        for (int x = 0, y = 0, i = 0; x < words.size() && y < punctuations.size(); ) {
            if (words.get(x).getStartIndex() == i) {
                txt = txt.concat(words.get(x).getValue() + " ");
                i = words.get(x).getEndIndex();
                x++;
            } else {
                if (punctuations.get(y).getStartIndex() == i) {
                    String value = punctuations.get(y).getValue();
                    txt = value.matches("[\\.,:\\?!=)%]") ? txt.concat(value + " ") : txt.substring(0, txt.length() - 1).concat(value);
                    i = punctuations.get(y).getEndIndex();
                    y++;
                } else i++;
            }

        }
        Iterator<PartOfText> codes = ((CodeHandler) codeHandler).getFragments().iterator();
        while (codes.hasNext())
            txt = txt.replaceAll("\0", "\n" + codes.next().getValue());
        return txt;
    }
}
