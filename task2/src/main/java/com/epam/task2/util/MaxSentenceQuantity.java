package com.epam.task2.util;

import com.epam.task2.entities.PartOfText;

import java.util.ArrayList;

/**
 * Created by a.gladkikh on 09/07/16.
 */
public class MaxSentenceQuantity {

    public static int doCounting(ArrayList<PartOfText> sentences) {
        int counter = 0;
        ArrayList<PartOfText> words;
        for (int i = 0; i < sentences.size() - 1; i++) {
            words = sentences.get(i).getComponents();
            for (int j = 0, y = 0; j < words.size() - 1; j++) {
                for (int k = j + 1; k < words.size() - 1; k++) {
                    if (words.get(j).getValue().equals(words.get(k).getValue())) {
                        counter++;
                        y++;
                        break;
                    }
                }
                if (y != 0)
                    break;
            }
        }

        return counter;
    }
}
