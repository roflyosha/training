package com.epam.task2.util;

import com.epam.task2.entities.PartOfText;

import java.util.Comparator;

/**
 * Created by a.gladkikh on 04/07/16.
 */
public class LetterMeetings implements Comparator<PartOfText> {
    private char letter;

    public LetterMeetings(char l) {
        letter = l;
    }

    int matchesCount(String s) {
        int found = 0;

        for (char c : s.toCharArray())
            if (letter == c)
                ++found;

        return found;
    }

    public int compare(PartOfText a, PartOfText b) {
        int diff = 0;
        if (a.getType().toString().equalsIgnoreCase("word") && b.getType().toString().equalsIgnoreCase("word")) {
            diff = matchesCount(a.getValue()) - matchesCount(b.getValue());
        }
        return (diff != 0) ? diff : (a.getValue()).compareTo(b.getValue());
    }
}
