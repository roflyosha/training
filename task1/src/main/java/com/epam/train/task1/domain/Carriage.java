package com.epam.train.task1.domain;

/**
 * Created by gladkikhA on 20/06/16.
 */
public abstract class Carriage {
    private int maxWeight;
    private String model;
    private int carriageId;

    public Carriage(int maxWeight, String model, int carriageId) {
        this.maxWeight = maxWeight;
        this.model = model;
        this.carriageId = carriageId;
    }

    @Override
    public String toString() {
        return "maxWeight=" + maxWeight +
                ", model='" + model + '\'' +
                ", carriageId=" + carriageId;
    }

    public int getMaxWeight() {
        return maxWeight;
    }

    public void setMaxWeight(int maxWeight) {
        this.maxWeight = maxWeight;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getCarriageId() {
        return carriageId;
    }

    public void setCarriageId(int carriageId) {
        this.carriageId = carriageId;
    }
}
