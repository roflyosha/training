package com.epam.train.task1.util;

import com.epam.train.task1.domain.Train;
import com.epam.train.task1.domain.impl.FreightCarriage;
import com.epam.train.task1.domain.impl.PassengerCarriage;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gladkikhA on 20/06/16.
 */
public class TrainUtils {
    private static final Logger log = Logger.getLogger(TrainUtils.class.getName());

    public static int totalPassengers(Train train) {
        List<PassengerCarriage> carriages = train.getPassengerCarriageList();
        int res = 0;
        for (PassengerCarriage c: carriages) {
            res += c.getPassengers();
        }
        return res;
    }

    public static int totalBaggage(Train train) {
        List<FreightCarriage> carriages = train.getFreightCarriageList();
        int res = 0;
        for (FreightCarriage c: carriages) {
            res += c.getBaggage();
        }
        return res;
    }

    public static List<PassengerCarriage> filterCarriages(Train train, int minPassengers, int maxPassengers) {
        List<PassengerCarriage> carriages = train.getPassengerCarriageList();
        List<PassengerCarriage> filtered = new ArrayList<>();
        if (carriages != null && minPassengers > 0 && maxPassengers > 0 && minPassengers <= maxPassengers) {
            for (PassengerCarriage c: carriages) {
                if (c.getPassengers() >= minPassengers && c.getPassengers() <= maxPassengers) {
                    filtered.add(c);
                }
            }
        } else {
            log.warn("Wrong search parameters");
        }
        return filtered;
    }
}
