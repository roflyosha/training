package com.epam.train.task1.domain.impl;

import com.epam.train.task1.domain.Carriage;

/**
 * Created by gladkikhA on 20/06/16.
 */
public class FreightCarriage extends Carriage {
    private int baggage;

    public FreightCarriage(int maxWeight, String model, int carriageId, int baggage) {
        super(maxWeight, model, carriageId);
        this.baggage = baggage;
    }

    @Override
    public String toString() {
        return "FreightCarriage{" +
                super.toString() +
                ", baggage=" + baggage +
                '}';
    }

    public int getBaggage() {
        return baggage;
    }

    public void setBaggage(int baggage) {
        this.baggage = baggage;
    }
}
