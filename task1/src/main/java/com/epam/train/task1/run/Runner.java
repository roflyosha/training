package com.epam.train.task1.run;

import com.epam.train.task1.domain.Train;
import com.epam.train.task1.domain.compare.PassengerCarriageComparator;
import com.epam.train.task1.domain.impl.FreightCarriage;
import com.epam.train.task1.domain.impl.PassengerCarriage;
import com.epam.train.task1.util.TrainUtils;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by gladkikhA on 20/06/16.
 */
public class Runner {
    private final static Logger log = Logger.getLogger(Runner.class.getName());

    public static void main(String[] args) {
        Train t = init();
        logTotalPassengers(t);
        logTotalBaggage(t);
        logFilter(t, 10, 50);
        logSorted(t);
    }

    private static Train init() {
        List<PassengerCarriage> pList = new ArrayList<>();
        pList.add(new PassengerCarriage(50000, "Basic", 12312, PassengerCarriage.ComfortLevel.ECONOMY, 56));
        pList.add(new PassengerCarriage(50000, "Basic", 12314, PassengerCarriage.ComfortLevel.ECONOMY, 56));
        pList.add(new PassengerCarriage(50000, "Basic", 12316, PassengerCarriage.ComfortLevel.ECONOMY, 56));
        pList.add(new PassengerCarriage(50000, "Basic", 12318, PassengerCarriage.ComfortLevel.ECONOMY, 56));
        pList.add(new PassengerCarriage(50000, "Basic+", 14311, PassengerCarriage.ComfortLevel.MIDDLE, 45));
        pList.add(new PassengerCarriage(50000, "Basic+", 14313, PassengerCarriage.ComfortLevel.MIDDLE, 45));
        pList.add(new PassengerCarriage(50000, "Basic+", 14315, PassengerCarriage.ComfortLevel.MIDDLE, 45));
        pList.add(new PassengerCarriage(50000, "Basic+", 14316, PassengerCarriage.ComfortLevel.MIDDLE, 45));
        pList.add(new PassengerCarriage(50000, "Basic++", 20152, PassengerCarriage.ComfortLevel.BUSINESS, 20));
        pList.add(new PassengerCarriage(50000, "Basic++", 20154, PassengerCarriage.ComfortLevel.BUSINESS, 20));
        pList.add(new PassengerCarriage(50000, "Basic++", 20156, PassengerCarriage.ComfortLevel.BUSINESS, 20));
        List<FreightCarriage> fList = new ArrayList<>();
        fList.add(new FreightCarriage(80000, "Mule", 100123, 400));
        fList.add(new FreightCarriage(80000, "Mule", 100124, 400));
        fList.add(new FreightCarriage(80000, "Mule", 100125, 400));
        fList.add(new FreightCarriage(80000, "Mule", 205228, 300));
        Train t = new Train(8000, pList, fList);
        return t;
    }

    private static void logTotalBaggage(Train t) {
        log.info("Total baggage - " + TrainUtils.totalBaggage(t));
    }

    private static void logTotalPassengers(Train t) {
        log.info("Total passengers - " + TrainUtils.totalPassengers(t));
    }

    private static void logFilter(Train t, int min, int max) {
        log.info("Logging filtered carriages");
        logList(TrainUtils.filterCarriages(t, min, max));
    }

    /*ee*/
    private static void logSorted(Train t) {
        log.info("Logging sorted carriages");
        List<PassengerCarriage> kant = t.getPassengerCarriageList();
        Collections.sort(kant, new PassengerCarriageComparator());
        logList(kant);
    }

    private static void logList(List<?> list) {
        for (Object o : list) {
            log.info(o.toString());
        }
    }
}
