package com.epam.train.task1.domain;

import com.epam.train.task1.domain.impl.FreightCarriage;
import com.epam.train.task1.domain.impl.PassengerCarriage;

import java.util.List;

/**
 * Created by gladkikhA on 20/06/16.
 */
public class Train {
    private int power;
    private List<PassengerCarriage> passengerCarriageList;
    private List<FreightCarriage> freightCarriageList;

    public Train(int power, List<PassengerCarriage> passengerCarriageList, List<FreightCarriage> freightCarriageList) {
        this.power = power;
        this.passengerCarriageList = passengerCarriageList;
        this.freightCarriageList = freightCarriageList;
    }

    @Override
    public String toString() {
        return "Train{" +
                "power=" + power +
                '}';
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public List<PassengerCarriage> getPassengerCarriageList() {
        return passengerCarriageList;
    }

    public void setPassengerCarriageList(List<PassengerCarriage> passengerCarriageList) {
        this.passengerCarriageList = passengerCarriageList;
    }

    public List<FreightCarriage> getFreightCarriageList() {
        return freightCarriageList;
    }

    public void setFreightCarriageList(List<FreightCarriage> freightCarriageList) {
        this.freightCarriageList = freightCarriageList;
    }
}
