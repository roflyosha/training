package com.epam.train.task1.domain.impl;

import com.epam.train.task1.domain.Carriage;

/**
 * Created by gladkikhA on 20/06/16.
 */
public class PassengerCarriage extends Carriage {
    private ComfortLevel comfortLevel;
    private int passengers;
    public PassengerCarriage(int maxWeight, String model, int carriageId, ComfortLevel comfortLevel, int passengers) {
        super(maxWeight, model, carriageId);
        this.comfortLevel = comfortLevel;
        this.passengers = passengers;
    }

    @Override
    public String toString() {
        return "PassengerCarriage{" +
                super.toString() +
                ", comfortLevel=" + comfortLevel +
                ", passengers=" + passengers +
                '}';
    }

    public ComfortLevel getComfortLevel() {
        return comfortLevel;
    }

    public void setComfortLevel(ComfortLevel comfortLevel) {
        this.comfortLevel = comfortLevel;
    }

    public int getPassengers() {
        return passengers;
    }

    public void setPassengers(int passengers) {
        this.passengers = passengers;
    }

    public enum ComfortLevel {
        ECONOMY, MIDDLE, BUSINESS
    }
}
