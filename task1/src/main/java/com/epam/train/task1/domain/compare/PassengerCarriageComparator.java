package com.epam.train.task1.domain.compare;

import com.epam.train.task1.domain.impl.PassengerCarriage;

import java.util.Comparator;

/**
 * Created by gladkikhA on 20/06/16.
 */
public class PassengerCarriageComparator implements Comparator<PassengerCarriage> {
    public int compare(PassengerCarriage o1, PassengerCarriage o2) {
        return o2.getComfortLevel().ordinal() - o1.getComfortLevel().ordinal();
    }
}
