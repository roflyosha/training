package servlet;

import dom.DOMParser;
import entity.Flowers;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import sax.MySAXParser;
import stax.MyStAXParser;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.List;

/**
 * Created by a.gladkikh on 25/07/16.
 */
public class MyServlet extends HttpServlet {
    private final static Logger logger = Logger.getLogger(MyServlet.class);

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        String typeParse = req.getParameter("parseMethod");
        Flowers flowers = null;
        List<Flowers.Flower> flowerList;
        switch (typeParse) {
            case "DOM":
                logger.info("DOM method was selected in index.jsp");
                flowers = DOMParser.parseByDOM(getServletContext().getRealPath("/") + "resources/flowers.xml");
                break;
            case "SAX":
                logger.info("SAX method was selected in index.jsp");
                flowers = MySAXParser.parseBySAX(getServletContext().getRealPath("/") + "resources/flowers.xml");
                break;
            case "StAX":
                logger.info("StAX method was selected in index.jsp");
                try {
                    flowers = MyStAXParser.parseByStAX(getServletContext().getRealPath("/") + "resources/flowers.xml");
                } catch (XMLStreamException e) {
                    logger.warn(e.getLocalizedMessage());
                }

                break;
            default:
                req.setAttribute("value", "nothing was checked");
                break;
        }
        req.setAttribute("name", "flowers.xml was parsed by " + typeParse + " parser");
        logger.info("Reception collection of Flowers");
        flowerList = flowers.getFlower();
        req.setAttribute("list", flowerList);
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/output.jsp");
        logger.info("Sending response to the output.jsp");
        dispatcher.forward(req, res);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.service(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.service(req, resp);
    }
}
