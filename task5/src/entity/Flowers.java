
package entity;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="flower" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="name">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;pattern value="[A-Z][A-Za-z\s]+"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="soil">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;enumeration value="podzolic"/>
 *                         &lt;enumeration value="sod-podzolic"/>
 *                         &lt;enumeration value="dirt"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="origin">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;pattern value="[A-Z][A-Za-z\s]+"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="visualParameters">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="leavesColor" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="flowerSize" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="growingTips">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="temperature" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                             &lt;element name="lighting">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                                   &lt;enumeration value="true"/>
 *                                   &lt;enumeration value="false"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="watering" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="multiplying">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;enumeration value="seed"/>
 *                         &lt;enumeration value="leaves"/>
 *                         &lt;enumeration value="cuttings"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *                 &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "flower"
})
@XmlRootElement(name = "flowers")
public class Flowers {

    protected List<Flower> flower;

    /**
     * Gets the value of the flower property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the flower property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFlower().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Flowers.Flower }
     *
     *
     */
    public List<Flower> getFlower() {
        if (flower == null) {
            flower = new ArrayList<Flower>();
        }
        return this.flower;
    }


    /**
     * <p>Java class for anonymous complex type.
     *
     * <p>The following schema fragment specifies the expected content contained within this class.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="name">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;pattern value="[A-Z][A-Za-z\s]+"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="soil">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;enumeration value="podzolic"/>
     *               &lt;enumeration value="sod-podzolic"/>
     *               &lt;enumeration value="dirt"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="origin">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;pattern value="[A-Z][A-Za-z\s]+"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="visualParameters">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="leavesColor" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="flowerSize" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="growingTips">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="temperature" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                   &lt;element name="lighting">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *                         &lt;enumeration value="true"/>
     *                         &lt;enumeration value="false"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="watering" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="multiplying">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;enumeration value="seed"/>
     *               &lt;enumeration value="leaves"/>
     *               &lt;enumeration value="cuttings"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *       &lt;/sequence>
     *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}ID" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     *
     *
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "name",
        "soil",
        "origin",
        "visualParameters",
        "growingTips",
        "multiplying"
    })
    public static class Flower {

        @XmlElement(required = true)
        protected String name;
        @XmlElement(required = true)
        protected String soil;
        @XmlElement(required = true)
        protected String origin;
        @XmlElement(required = true)
        protected Flowers.Flower.VisualParameters visualParameters;
        @XmlElement(required = true)
        protected Flowers.Flower.GrowingTips growingTips;
        @XmlElement(required = true)
        protected String multiplying;
        @XmlAttribute(name = "id", required = true)
        @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
        @XmlID
        @XmlSchemaType(name = "ID")
        protected String id;

        @Override
        public String toString() {
            return "Flower{" +
                    "name='" + name + '\'' +
                    ", soil='" + soil + '\'' +
                    ", origin='" + origin + '\'' +
                    ", visualParameters=" + visualParameters +
                    ", growingTips=" + growingTips +
                    ", multiplying='" + multiplying + '\'' +
                    ", id='" + id + '\'' +
                    '}';
        }

        /**
         * Gets the value of the name property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getName() {
            return name;
        }

        /**
         * Sets the value of the name property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setName(String value) {
            this.name = value;
        }

        /**
         * Gets the value of the soil property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getSoil() {
            return soil;
        }

        /**
         * Sets the value of the soil property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setSoil(String value) {
            this.soil = value;
        }

        /**
         * Gets the value of the origin property.
         *
         * @return
         *     possible object is
         *     {@link String }
         *
         */
        public String getOrigin() {
            return origin;
        }

        /**
         * Sets the value of the origin property.
         *
         * @param value
         *     allowed object is
         *     {@link String }
         *
         */
        public void setOrigin(String value) {
            this.origin = value;
        }

        /**
         * Gets the value of the visualParameters property.
         *
         * @return
         *     possible object is
         *     {@link Flowers.Flower.VisualParameters }
         *
         */
        public Flowers.Flower.VisualParameters getVisualParameters() {
            return visualParameters;
        }

        /**
         * Sets the value of the visualParameters property.
         *
         * @param value
         *     allowed object is
         *     {@link Flowers.Flower.VisualParameters }
         *
         */
        public void setVisualParameters(Flowers.Flower.VisualParameters value) {
            this.visualParameters = value;
        }

        /**
         * Gets the value of the growingTips property.
         *
         * @return
         *     possible object is
         *     {@link Flowers.Flower.GrowingTips }
         *
         */
        public Flowers.Flower.GrowingTips getGrowingTips() {
            return growingTips;
        }

        /**
         * Sets the value of the growingTips property.
         *
         * @param value
         *     allowed object is
         *     {@link Flowers.Flower.GrowingTips }
         *
         */
        public void setGrowingTips(Flowers.Flower.GrowingTips value) {
            this.growingTips = value;
        }

        /**
         * Gets the value of the multiplying property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMultiplying() {
            return multiplying;
        }

        /**
         * Sets the value of the multiplying property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMultiplying(String value) {
            this.multiplying = value;
        }

        /**
         * Gets the value of the id property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getId() {
            return id;
        }

        /**
         * Sets the value of the id property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setId(String value) {
            this.id = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="temperature" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *         &lt;element name="lighting">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
         *               &lt;enumeration value="true"/>
         *               &lt;enumeration value="false"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="watering" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "temperature",
            "lighting",
            "watering"
        })
        public static class GrowingTips {

            protected int temperature;
            @XmlElement(required = true)
            protected String lighting;
            protected int watering;

            /**
             * Gets the value of the temperature property.
             * 
             */
            public int getTemperature() {
                return temperature;
            }

            /**
             * Sets the value of the temperature property.
             * 
             */
            public void setTemperature(int value) {
                this.temperature = value;
            }

            /**
             * Gets the value of the lighting property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLighting() {
                return lighting;
            }

            /**
             * Sets the value of the lighting property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLighting(String value) {
                this.lighting = value;
            }

            /**
             * Gets the value of the watering property.
             * 
             */
            public int getWatering() {
                return watering;
            }

            /**
             * Sets the value of the watering property.
             * 
             */
            public void setWatering(int value) {
                this.watering = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="leavesColor" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="flowerSize" type="{http://www.w3.org/2001/XMLSchema}int"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "leavesColor",
            "flowerSize"
        })
        public static class VisualParameters {

            @XmlElement(required = true)
            protected String leavesColor;
            protected int flowerSize;

            /**
             * Gets the value of the leavesColor property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLeavesColor() {
                return leavesColor;
            }

            /**
             * Sets the value of the leavesColor property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLeavesColor(String value) {
                this.leavesColor = value;
            }

            /**
             * Gets the value of the flowerSize property.
             * 
             */
            public int getFlowerSize() {
                return flowerSize;
            }

            /**
             * Sets the value of the flowerSize property.
             * 
             */
            public void setFlowerSize(int value) {
                this.flowerSize = value;
            }

        }

    }

}
