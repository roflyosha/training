package sax;

import entity.Flowers;
import entity.ObjectFactory;
import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;

/**
 * Created by a.gladkikh on 16/07/16.
 */
public class MySAXParser extends DefaultHandler {
    private final static Logger logger = Logger.getLogger(MySAXParser.class);
    private static ObjectFactory factory = new ObjectFactory();
    private static Flowers flowers = factory.createFlowers();
    private String id = "";
    private String tagName = "";
    private Flowers.Flower flower;

    public static Flowers parseBySAX(String file) {
        logger.info("Parsing XML file by SAX parser");
        try {
            flowers.getFlower().removeAll(flowers.getFlower());
            SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
            SAXParser saxParser = saxParserFactory.newSAXParser();
            saxParser.parse(file, new MySAXParser());

        } catch (IOException e) {
            logger.warn(e.getLocalizedMessage());
        } catch (ParserConfigurationException e) {
            logger.warn(e.getLocalizedMessage());
        } catch (SAXException e) {
            logger.warn(e.getLocalizedMessage());
        }
        return flowers;
    }

    @Override
    public void startDocument() throws SAXException {
        logger.info("Start of parsing XML document...");
    }

    @Override
    public void endDocument() throws SAXException {
        logger.info("End of parsing XML document...");
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws
            SAXException {
        tagName = qName;
        if (qName.equals("flower")) {
            flower = factory.createFlowersFlower();
            logger.info("Creating object Flower...");
            flowers.getFlower().add(flower);
            Flowers.Flower.VisualParameters visualParameters = factory.createFlowersFlowerVisualParameters();
            Flowers.Flower.GrowingTips growingTips = factory.createFlowersFlowerGrowingTips();
            id = attributes.getValue(0);
            logger.info("Flower ID: " + id);
            flower.setId(id);
            flower.setVisualParameters(visualParameters);
            flower.setGrowingTips(growingTips);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        tagName = "";
        id = "";

    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String str = new String(ch, start, length);
        if (tagName.equals("name")) {
            flower.setName(str);
            logger.info("Name: " + new String(ch, start, length));
        }
        if (tagName.equals("soil")) {
            flower.setSoil(str);
            logger.info("Soil: " + new String(ch, start, length));
        }
        if (tagName.equals("origin")) {
            flower.setOrigin(str);
            logger.info("Origin: " + new String(ch, start, length));
        }
        if (tagName.equals("leavesColor")) {
            flower.getVisualParameters().setLeavesColor(str);
            logger.info("Leaves color: " + new String(ch, start, length));
        }
        if (tagName.equals("flowerSize")) {
            flower.getVisualParameters().setFlowerSize(Integer.parseInt(str));
            logger.info("Flower size: " + new String(ch, start, length));
        }
        if (tagName.equals("temperature")) {
            flower.getGrowingTips().setTemperature(Integer.parseInt(str));
            logger.info("Temperature: " + new String(ch, start, length));
        }
        if (tagName.equals("lighting")) {
            flower.getGrowingTips().setLighting(str);
            logger.info("Lighting: " + new String(ch, start, length));
        }
        if (tagName.equals("watering")) {
            flower.getGrowingTips().setWatering(Integer.parseInt(str));
            logger.info("Watering: " + new String(ch, start, length));
        }
        if (tagName.equals("multiplying")) {
            flower.setMultiplying(str);
            logger.info("Multiplying: " + new String(ch, start, length));
        }

    }
}
