package dom;

import entity.Flowers;
import entity.ObjectFactory;
import org.apache.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

/**
 * Created by a.gladkikh on 12/07/16.
 */
public class DOMParser {
    private final static Logger logger = Logger.getLogger(DOMParser.class);
    private static ObjectFactory factory = new ObjectFactory();
    private static Flowers flowers = factory.createFlowers();
    private static Flowers.Flower flower;


    public static Flowers parseByDOM(String file) {
        flowers.getFlower().removeAll(flowers.getFlower());
        logger.info("Parsing XML file by DOM parser");
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            org.w3c.dom.Document document = documentBuilder.parse(file);
            NodeList nodeList = document.getDocumentElement().getChildNodes();
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    flower = factory.createFlowersFlower();
                    logger.info("Creating object Flower...");
                    flowers.getFlower().add(flower);
                    Flowers.Flower.VisualParameters visualParameters = factory.createFlowersFlowerVisualParameters();
                    Flowers.Flower.GrowingTips growingTips = factory.createFlowersFlowerGrowingTips();
                    flower.setVisualParameters(visualParameters);
                    flower.setGrowingTips(growingTips);
                    flower.setId(element.getAttribute("id"));
                    logger.info(element.getTagName().toUpperCase() + ", id = " + element.getAttribute("id"));
                    NodeList nodeList1 = element.getChildNodes();
                    for (int j = 0; j < nodeList1.getLength(); j++) {
                        Node node1 = nodeList1.item(j);
                        if (node1.getNodeType() == Node.ELEMENT_NODE) {
                            String content = node1.getLastChild().getTextContent().trim();
                            switch (node1.getNodeName()) {
                                case "name":
                                    flower.setName(content);
                                    logger.info("Name: " + content);
                                    break;
                                case "soil":
                                    flower.setSoil(content);
                                    logger.info("Soil: " + content);
                                    break;
                                case "origin":
                                    flower.setOrigin(content);
                                    logger.info("Origin: " + content);
                                    break;
                                case "visualParameters":
                                    NodeList nodeList2 = ((Element) node1).getChildNodes();
                                    for (int x = 0; x < nodeList2.getLength(); x++) {
                                        Node node2 = nodeList2.item(x);
                                        if (node2.getNodeType() == Node.ELEMENT_NODE) {
                                            String content1 = node2.getLastChild().getTextContent().trim();
                                            switch (node2.getNodeName()) {
                                                case "leavesColor":
                                                    flower.getVisualParameters().setLeavesColor(content1);
                                                    logger.info("Leaves color: " + content1);
                                                    break;
                                                case "flowerSize":
                                                    flower.getVisualParameters().setFlowerSize(Integer.parseInt(content1));
                                                    logger.info("Flower size: " + content1);
                                                    break;
                                            }
                                        }
                                    }
                                    break;
                                case "growingTips":
                                    NodeList nodeList3 = ((Element) node1).getChildNodes();
                                    for (int y = 0; y < nodeList3.getLength(); y++) {
                                        Node node3 = nodeList3.item(y);
                                        if (node3.getNodeType() == Node.ELEMENT_NODE) {
                                            String content2 = node3.getLastChild().getTextContent().trim();
                                            switch (node3.getNodeName()) {
                                                case "temperature":
                                                    flower.getGrowingTips().setTemperature(Integer.parseInt(content2));
                                                    logger.info("Temperature: " + content2);
                                                    break;
                                                case "lighting":
                                                    flower.getGrowingTips().setLighting(content2);
                                                    logger.info("Lighting: " + content2);
                                                    break;
                                                case "watering":
                                                    flower.getGrowingTips().setWatering(Integer.parseInt(content2));
                                                    logger.info("Watering: " + content2);
                                            }
                                        }
                                    }
                                    break;
                                case "multiplying":
                                    flower.setMultiplying(content);
                                    logger.info("Multiplying: " + content);
                                    break;
                            }

                        }
                    }
                }
            }

        } catch (ParserConfigurationException e) {
            logger.warn(e.getLocalizedMessage());
        } catch (SAXException e) {
            logger.warn(e.getLocalizedMessage());
        } catch (IOException e) {
            logger.warn(e.getLocalizedMessage());
        }
        return flowers;
    }


}
