package stax;

import entity.Flowers;
import entity.ObjectFactory;
import org.apache.log4j.Logger;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Created by a.gladkikh on 17/07/16.
 */
public class MyStAXParser {
    private final static Logger logger = Logger.getLogger(MyStAXParser.class);
    private static ObjectFactory factory = new ObjectFactory();
    private static Flowers flowers = factory.createFlowers();
    private static Flowers.Flower flower;

    public static Flowers parseByStAX(String file) throws FileNotFoundException, XMLStreamException {
        flowers.getFlower().removeAll(flowers.getFlower());
        logger.info("Parsing XML file by StAX parser");
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(file));
        while (xmlEventReader.hasNext()) {
            XMLEvent xmlEvent = xmlEventReader.nextEvent();
            if (xmlEvent.isStartElement()) {
                StartElement startElement = xmlEvent.asStartElement();
                switch (startElement.getName().getLocalPart()) {
                    case "flower":
                        flower = factory.createFlowersFlower();
                        logger.info("Creating object Flower...");
                        flowers.getFlower().add(flower);
                        Flowers.Flower.VisualParameters visualParameters = factory.createFlowersFlowerVisualParameters();
                        Flowers.Flower.GrowingTips growingTips = factory.createFlowersFlowerGrowingTips();
                        flower.setVisualParameters(visualParameters);
                        flower.setGrowingTips(growingTips);
                        String id = startElement.getAttributeByName(new QName("id")).getValue();
                        flower.setId(id);
                        logger.info("Flower ID: " + id);
                        break;
                    case "name":
                        xmlEvent = xmlEventReader.nextEvent();
                        String name = xmlEvent.asCharacters().getData();
                        flower.setName(name);
                        logger.info("Name: " + name);
                        break;
                    case "soil":
                        xmlEvent = xmlEventReader.nextEvent();
                        String soil = xmlEvent.asCharacters().getData();
                        flower.setSoil(soil);
                        logger.info("Soil: " + soil);
                        break;
                    case "origin":
                        xmlEvent = xmlEventReader.nextEvent();
                        String origin = xmlEvent.asCharacters().getData();
                        flower.setOrigin(origin);
                        logger.info("Origin: " + origin);
                        break;
                    case "leavesColor":
                        xmlEvent = xmlEventReader.nextEvent();
                        String leavesColor = xmlEvent.asCharacters().getData();
                        flower.getVisualParameters().setLeavesColor(leavesColor);
                        logger.info("Leaves color: " + leavesColor);
                        break;
                    case "flowerSize":
                        xmlEvent = xmlEventReader.nextEvent();
                        String flowerSize = xmlEvent.asCharacters().getData();
                        flower.getVisualParameters().setFlowerSize(Integer.parseInt(flowerSize));
                        logger.info("Flower size: " + flowerSize);
                        break;
                    case "temperature":
                        xmlEvent = xmlEventReader.nextEvent();
                        String temperature = xmlEvent.asCharacters().getData();
                        flower.getGrowingTips().setTemperature(Integer.parseInt(temperature));
                        logger.info("Temperature: " + temperature);
                        break;
                    case "lighting":
                        xmlEvent = xmlEventReader.nextEvent();
                        String lighting = xmlEvent.asCharacters().getData();
                        flower.getGrowingTips().setLighting(lighting);
                        logger.info("Lighting: " + lighting);
                        break;
                    case "watering":
                        xmlEvent = xmlEventReader.nextEvent();
                        String watering = xmlEvent.asCharacters().getData();
                        flower.getGrowingTips().setWatering(Integer.parseInt(watering));
                        logger.info("Watering: " + watering);
                        break;
                    case "multiplying":
                        xmlEvent = xmlEventReader.nextEvent();
                        String multiplying = xmlEvent.asCharacters().getData();
                        flower.setMultiplying(multiplying);
                        logger.info("Multiplying: " + multiplying);
                        break;
                }
            }
        }
        return flowers;
    }

}
