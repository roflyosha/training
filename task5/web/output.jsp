<%--
  Created by IntelliJ IDEA.
  User: a.gladkikh
  Date: 25/07/16
  Time: 13:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="resources/main.css">
</head>
<body>
<a href="index.jsp">&lt;&lt;Вернуться в меню</a>
<table>
    <caption>${name}</caption>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Soil</th>
        <th>Origin</th>
        <th>Leaves color</th>
        <th>Flower size</th>
        <th>Temperature</th>
        <th>Lighting</th>
        <th>Watering</th>
        <th>Multiplying</th>
    </tr>
    <c:forEach items="${list}" var="item">
        <tr>
            <td>${item.getId()}</td>
            <td>${item.getName()}</td>
            <td>${item.getSoil()}</td>
            <td>${item.getOrigin()}</td>
            <td>${item.getVisualParameters().getLeavesColor()}</td>
            <td>${item.getVisualParameters().getFlowerSize()}</td>
            <td>${item.getGrowingTips().getTemperature()}</td>
            <td>${item.getGrowingTips().getLighting()}</td>
            <td>${item.getGrowingTips().getWatering()}</td>
            <td>${item.getMultiplying()}</td>
        </tr>
    </c:forEach>

</table>
</body>
</html>
